<?php include('_includes/header.php'); ?>

  <nav class="breadcrumb  container">
    <ol class="breadcrumb__list">
      <li><a href="">Home</a></li>
      <li>Products</li>
    </ol>
  </nav>

  <div class="container">
    <ul class="product-list  grid grid--tiny"  data-product-list>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/1.jpg" alt="Footpegs">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Footpegs</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/2.jpg" alt="Pivot levers">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Pivot levers</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/3.jpg" alt="Triple clamps">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Triple clamps</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/4.jpg" alt="Triple clamps accessories">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Triple clamps accessories</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/5.jpg" alt="Handlebars">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Handlebars</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/6.jpg" alt="Parts &amp; accessories">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Parts &amp; accessories</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/7.jpg" alt="Grips">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Grips</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/8.jpg" alt="Tools">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Tools</button>
          </div>
        </a>
      </li>
      <li class="product-list__product  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <a class="product-card" href="">
          <div class="product-card__img">
            <img src="assets/img/products/9.jpg" alt="Clearance">
          </div>
          <div class="product-card__details"  data-equal-height-element>
            <button class="btn btn--full btn--hollow">Clearance</button>
          </div>
        </a>
      </li>
    </ul>
  </div>

<?php include('_includes/footer.php'); ?>