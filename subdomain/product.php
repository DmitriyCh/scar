<?php include('_includes/header.php'); ?>

  <nav class="breadcrumb  container">
    <ol class="breadcrumb__list">
      <li><a href="">Home</a></li>
      <li><a href="">Products</a></li>
      <li><a href="">Footpegs</a></li>
      <li>Evolution footpegs</li>
    </ol>
  </nav>

  <section class="product-view">
    <div class="container">
      <div class="product-view__grid  grid grid--large">
        <div class="1/2  grid__cell">
          <div class="alpha  product-phone-title">Evolution footpegs</div>
          <div class="product-images">
            <div class="product-images__lead-image">
              <img src="assets/img/product/lead.jpg" alt="Footpegs">
            </div>
            <ul class="product-images__thumbnails">
              <li class="product-images__thumbnail"><a href=""><img src="assets/img/product/1.jpg" alt="Footpegs"></a></li>
              <li class="product-images__thumbnail"><a href="" class="active"><img src="assets/img/product/2.jpg" alt="Footpegs"></a></li>
              <li class="product-images__thumbnail"><a href=""><img src="assets/img/product/3.jpg" alt="Footpegs"></a></li>
              <li class="product-images__thumbnail"><a href=""><img src="assets/img/product/4.jpg" alt="Footpegs"></a></li>
              <li class="product-images__thumbnail"><a href=""><img src="assets/img/product/5.jpg" alt="Footpegs"></a></li>
              <li class="product-images__thumbnail"><a href=""><img src="assets/img/product/6.jpg" alt="Footpegs"></a></li>
            </ul>
          </div>
        </div>
        <div class="1/2  grid__cell">
          <div class="product-view__details">
            <h1 class="product-view__name">Evolution footpegs</h1>
            <div class="product-view__price">399.99€</div>
            <div class="product-view__region-prices">(Approx £300.99GBP / $525.99USD / $699.95AUD)</div>
            <div class="product-view__variations">
              <div class="product-view__variation">
                Make
                <select>
                  <option disabled selected>-Select-</option>
                  <option>Honda</option>
                  <option>Yamaha</option>
                  <option>KTM</option>
                </select>
              </div>
              <div class="product-view__variation">
                Model
                <select disabled>
                  <option disabled selected>-Select-</option>
                </select>
              </div>
              <div class="product-view__variation">
                Year
                <select disabled>
                  <option disabled selected>-Select-</option>
                </select>
              </div>
              <div class="product-view__variation">
                Colour
                <select disabled>
                  <option disabled selected>-Select-</option>
                </select>
              </div>
            </div>
            <a class="product-view__add-to-cart-btn  btn btn--full btn--arrow" href="">Add to cart</a>
            <hr>
            <div class="product-view__description">
              <p><strong>Description</strong></p>
              <ul>
                <li>Top Triple Clamp</li>
                <li>Bottom Triple Clamp</li>
                <li>Pressed in stem and “All Balls” Lower bearing</li>
              </ul>
              <p>Lightest product on the market due to innovative construction. Made from high quality aircraft aluminium Cnc Machined 7075 T6. Top clamps offer 4 bar positions with our bar mounts. Easy to install with its pre-pressed stem, seal and bearing.</p>
              <p>Strength and rigidity added. Better control &amp; and greater rigidity and less shock and vibration. Offset changes allow to improve straight-line stability or a better turning ability.</p>
              <p>Several years of factory testing. Motocross Triple clamps are anodized to prevent corrosion. It is very easy to set the Triple Clamps because the aluminium stem and bearing is pre-assembled.</p>
            </div>
            <hr>
            <a href="" class="uppercase"><small><strong>Download application chart (PDF)</strong></small></a>
            <hr>
            <h5>Related items</h5>
            <ul class="related-items  grid grid--small">
              <li class="1/2--lap-and-up  grid__cell">
                <a class="related-item  product-card" href="">
                  <div class="product-card__img">
                    <img src="assets/img/product/related1.jpg" alt="Footpeg foam">
                  </div>
                  <div class="product-card__details">
                    <span class="product-card__title">Footpeg foam</span>
                    <span class="product-card__price">399.99€</span>
                  </div>
                </a>
              </li>
              <li class="1/2--lap-and-up  grid__cell">
                <a class="related-item  product-card" href="">
                  <div class="product-card__img">
                    <img src="assets/img/product/related2.jpg" alt="Evolution footpegs">
                  </div>
                  <div class="product-card__details">
                    <span class="product-card__title">Evolution footpegs</span>
                    <span class="product-card__price">399.99€</span>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('_includes/footer.php'); ?>