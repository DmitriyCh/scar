var scar = scar || {};

! function() {
  "use strict";
  
  scar.MasonryGallery = function(options) {

    function _setupMasonryGallery() {
      var $gallery = options.$context.masonry({
        itemSelector: options.itemSelector,
        columnWidth: options.columnWidth,
        percentPosition: true
      });
      $gallery.imagesLoaded().progress( function() {
        $gallery.masonry('layout');
      });
    }

    function _addFeatherlightCaptions() {
      $.featherlight.prototype.afterContent = function() {
        var caption = this.$currentTarget.find('img').attr('alt');
        this.$instance.find('.caption').remove();
        $('<div class="featherlight-caption">').text(caption).prependTo(this.$instance.find('.featherlight-content'));
      };
    }

    function init() {
      _setupMasonryGallery();
      _addFeatherlightCaptions();
    }

    return {
      init: init
    };
  };
}();