var scar = scar || {};

! function() {
  "use strict";
  
  scar.PartsFinder = function(options) {

    function _clickHandler(){
      options.$context.toggleClass(options.navActiveClass);
    }

    function _eventsHandler(){
      options.$menuButton.on('click', function(e){
        e.preventDefault();
        _clickHandler(e, $(this));
        console.log('tom');
      });
    }

    function init() {
      _eventsHandler();
    }

    return {
      init: init
    };
  };
}();