var scar = scar || {};

! function() {
  "use strict";
  
  scar.EqualHeight = function(options) {

    function _applyEqualHeight() {
      var elements = $(options.elementSelector, options.$context);

      var elementHeights = elements.map(function() {
        return $(this).height();
      }).get();

      var maxHeight = Math.max.apply(null, elementHeights);

      elements.height(maxHeight);
      // elements.css('min-height', maxHeight);
    }

    function init() {
      _applyEqualHeight();
    }

    return {
      init: init
    };
  };
}();