var scar = scar || {};

! function() {
  
  "use strict";
  
  scar.constants = {};
  scar.init = function() {

    // Fastclick for mobile device touches
    if ('addEventListener' in document) {
      document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
      }, false);
    }
      
    // var navMenuContext = $("[data-nav-menu]");
    // navMenuContext.length > 0 && $.each(navMenuContext, function(){
    //   var navMenuModule = new jdj.NavMenu({
    //     $context:         $(this),
    //     $menuButton:      $('[data-nav-menu-trigger]'),
    //     $closeButton:     $('[data-nav-menu-close-button]'),
    //     htmlActiveClass:  'is-menu-open',
    //     navActiveClass:   'is-visible'
    //   });
    //   navMenuModule.init();
    // });

    var partsFinderContext = $("[data-parts-finder]");
    partsFinderContext.length > 0 && $.each(partsFinderContext, function(){
      var partsFinderModule = new scar.PartsFinder({
        $context:         $(this),
        $menuButton:      $('[data-parts-finder-trigger]'),
        navActiveClass:   'is-visible'
      });
      partsFinderModule.init();
    });

    var productsListContext = $("[data-product-list]");
    productsListContext.length > 0 && $.each(productsListContext, function(){
      var equalHeightModule = new scar.EqualHeight({
        $context:               $(this),
        elementSelector:        '[data-equal-height-element]'
      });
      equalHeightModule.init();
    });

    var masonryGalleryContext = $("[data-masonry-gallery]");
    masonryGalleryContext.length > 0 && $.each(masonryGalleryContext, function(){
      var masonryGalleryModule = new scar.MasonryGallery({
        $context:         $(this),
        itemSelector:     '[data-masonry-gallery-item]',
        columnWidth:      '[data-masonry-gallery-sizer]'
      });
      masonryGalleryModule.init();
    });

  }();
}();