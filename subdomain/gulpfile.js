var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    livereload = require('gulp-livereload'),
    autoprefixer = require('gulp-autoprefixer');

// Asset paths
var paths = {
  sass:   './assets/scss/**/*.scss',
  css:    './assets/css',
  jsSrc:  [
            './assets/js/vendor/modernizr-custom.min.js',
            './assets/js/vendor/fastclick.js',
            './assets/js/vendor/masonry.pkgd.min.js',
            './assets/js/vendor/imagesloaded.pkgd.min.js',
            './assets/js/vendor/featherlight.min.js',

            './assets/js/modules/*.js',
            './assets/js/app.js',

            '!./assets/js/vendor/jquery-3.0.0.min.js',
            '!./assets/js/**/_*.js'
          ],
  jsDest: './assets/js'
};

// Plugin options
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'IE 10']
};

// TASK: Compile Sass
gulp.task('sass', function () {
  gulp.src(paths.sass)
    .pipe(plumber())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(paths.css))
    .pipe(livereload());
});

// TASK: Concatenate and minify JS
gulp.task('scripts', function() {
  gulp.src(paths.jsSrc)
    .pipe(plumber())
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.jsDest));
    // .pipe(livereload());
});

// TASK: Create LiveReload
gulp.task('livereload', function() {
  livereload.listen({ host: 'scarracing.dev', port: 12345 });
});

// TASK: Watch for changes
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.jsSrc, ['scripts']);
});

// TASK: Default. Just run 'gulp' in CLI
gulp.task('default', ['livereload', 'watch']);