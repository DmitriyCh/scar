<?php include('_includes/header.php'); ?>

  <nav class="breadcrumb  container">
    <ol class="breadcrumb__list">
      <li><a href="">Home</a></li>
      <li>Worldwide distributors</li>
    </ol>
  </nav>

  <div class="container">
        
    <div class="dealers-map" id="dealersMap"></div>

    <h1>Worldwide distributors</h1>

    <ul class="dealers-list  grid grid--tiny">
      <li class="dealers-list__dealer  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <div class="flag">
          <div class="flag__img">
            <span class="location-pin">1</span>
          </div>
          <div class="flag__body">
            <strong>Austria</strong>
          </div>
        </div>
        Auner GmbH
        <br>T: (+43) 2252259459
        <br>F: (+43)225225945930
        <br>E: <a href="mailto:auner@aon.at">auner@aon.at</a>
        <br><a href="http://www.auner.at" target="_blank">www.auner.at</a>
      </li>
      <li class="dealers-list__dealer  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <div class="flag">
          <div class="flag__img">
            <span class="location-pin">2</span>
          </div>
          <div class="flag__body">
            <strong>Belgium</strong>
          </div>
        </div>
        Rino Trading
        <br>T: (+32) 15 228 433
        <br>F: (+32) 15 230 628
        <br>E: <a href="mailto:info@rinotrading.be">info@rinotrading.be</a>
        <br><a href="http://www.rinotrading.be" target="_blank">www.rinotrading.be</a>
      </li>
      <li class="dealers-list__dealer  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <div class="flag">
          <div class="flag__img">
            <span class="location-pin">3</span>
          </div>
          <div class="flag__body">
            <strong>Canada</strong>
          </div>
        </div>
        Parts Canada
        <br>T: 1-888-627-5495
        <br>F: (519) 644-2672
        <br>E: <a href="mailto:information@partscanada.com">information@partscanada.com</a>
        <br><a href="http://www.partscanada.com" target="_blank">www.partscanada.com</a>
      </li>
      <li class="dealers-list__dealer  1/2 1/3--handheld 1/4--lap-and-up grid__cell">
        <div class="flag">
          <div class="flag__img">
            <span class="location-pin">3</span>
          </div>
          <div class="flag__body">
            <strong>Denmark</strong>
          </div>
        </div>
        Duells
        <br>T: (+45) 26235670
        <br>E: <a href="mailto:info@duells.dk">info@duells.dk</a>
        <br><a href="http://www.duells.dk" target="_blank">www.duells.dk</a>
      </li>
    </ul>

  </div>

  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDdNzktGBkQgwSCS_Wq8Eu3VuTliQep9bk"></script>
  <script>
    var locations = [
      [
        "Auner GmbH",
        "Kärntner Str. 141, 8053 Graz, Austria",
        "47.048309",
        "15.415745"
      ],
      [
        "Rino Trading",
        "Industriepark 29, 2235 Hulshout, Belgium",
        "51.086153",
        "4.815120"
      ],
      [
        "Parts Canada",
        "3935 Cheese Factory Rd, London, ON N6N 1G2, Canada",
        "42.932900",
        "-81.160800"
      ],
      [
        "Duells",
        "Jernbanegade 23 B, 4000 Roskilde, Denmark",
        "55.637623",
        "12.078126"
      ]
    ];

    var map = new google.maps.Map(document.getElementById('dealersMap'), {
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker, i;
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
        map: map,
        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + (i+1) + '|FF9999|FFFFFF'
      });

      bounds.extend(marker.position);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0] + "<br>" + locations[i][1]);
          infowindow.open(map, marker);
        }
      })(marker, i));

      map.fitBounds(bounds);
    }
  </script>

<?php include('_includes/footer.php'); ?>