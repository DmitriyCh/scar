<?php include('_includes/header.php'); ?>

  <nav class="breadcrumb  container">
    <ol class="breadcrumb__list">
      <li><a href="">Home</a></li>
      <li>Company</li>
    </ol>
  </nav>

  <div class="container">

    <div class="image-cut-in">
      <img class="image-cut-in__img" src="assets/img/about-banner.jpg" alt="Scar Racing. Founded 2003">
      <div class="image-cut-in__content">
        <h1 class="image-cut-in__heading">Since 2003,</h1>
        <div class="image-cut-in__body">
          <p>Scar is the brandname specialized in high speed aluminium spare parts manufactured for the motorcycling market. In the past, we produced and developed triple clamps for some of the biggest companies in the motorcycling market, and have equipped several bikes with OEM equipments. Today, Scar establishes itself as the European leader of triple clamps, and as the World leader of aluminium Footpegs with steel inset teeth. The Scar team continuous innovation ensures our ability to give to the rider high performance and top quality products for their success.</p>
          <h5>The World's fastest riders have used Scar products...</h5>
          <p>Ken Roczen, Christophe Pourcel, Sebastien Pourcel, Max Anstie, Arnaud Tonus, Christophe Charlier, Florent Richier, Gregory Aranda, Loic larrieu, David Vuillemin, Mickael Maschio, Pierre Alexandre Renet, Stéphane Peterhansel, Jean Michel Bayle, Julien Bill, Andreas Boller, Avo Leok, Jiri Cepelak, Damon Graulus, Stéphane Blot, Valentin Teillet, Arnaud Demesteer, Scott Elderfield, Gordon Crockard, Remi Bizouard, Livia Lancelot, Marc Germain, Frederic Vialle, Yann Guedard, Rodolphe Beau, Nicolas Cailly, Sebastien Guillaume, Khounsith Vongsana, Jason Clermont, Jeremy Tarroux, Yohan Lafont, Robin Kappel, Melvin Regner, Sebastien Trottier, Mathias Bellino, Damien Heron, Steven Lenoir, Maxime Lesage, Daniel Obelisco, Julien Miretti, Ilkka Salo, Thomas Chareyre.</p>
        </div>
      </div>
    </div>

  </div>

<?php include('_includes/footer.php'); ?>