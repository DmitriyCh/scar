  <footer class="footer">
    <div class="container">

      <div class="footer__social">
        <h3>Follow Scar</h3>
        <a class="social-icon icon--facebook">Facebook</a>
        <a class="social-icon icon--twitter">Twitter</a>
        <a class="social-icon icon--instagram">Instagram</a>
        <a class="social-icon icon--linkedin">LinkedIn</a>
        <a class="social-icon icon--rss">RSS</a>
      </div>

      <form class="newsletter-signup">
        <h3>Sign up for news, events and offers</h3>
        <fieldset>
          <input class="newsletter-signup__input  text-input" type="text" placeholder="Name">
          <input class="newsletter-signup__input  text-input" type="email" placeholder="Email">
          <input class="newsletter-signup__input  text-input" type="text" placeholder="Country">
          <button class="newsletter-signup__submit  btn">Sign up</button>
        </fieldset>
      </form>

      <nav class="footer__nav">
        <ul>
          <li><a href="">Home</a></li>
          <li><a href="">Products</a></li>
          <li><a href="">News</a></li>
          <li><a href="">Dealers</a></li>
          <li><a href="">Riders</a></li>
          <li><a href="">Downloads</a></li>
          <li><a href="">Basket</a></li>
          <li><a href="">Company</a></li>
          <li><a href="">Contact</a></li>
          <li><a href="">Newsletter</a></li>
          <li><a href="">Terms &amp; Conditions</a></li>
          <li><a href="">FAQs</a></li>
        </ul>
      </nav>

      <div class="footer__copyright">
        &copy;&nbsp;2016&nbsp;Scar&nbsp;Racing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Design&nbsp;by&nbsp;<a href="http://www.joemillercreative.com" target="_blank">Joe&nbsp;Miller&nbsp;Creative</a>.&nbsp;Build&nbsp;by&nbsp;<a href="http://twitter.com/colourgarden" target="_blank">Tom&nbsp;Hare</a>.
      </div>

    </div>
  </footer>

<script src="assets/js/vendor/jquery-3.0.0.min.js"></script>
<script src="assets/js/app.min.js"></script>

<script src="//scarracing.dev:12345/livereload.js?snipver=1" async defer></script>

</body>
</html>