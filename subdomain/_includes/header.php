<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Scar Racing</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700">
  <link rel="stylesheet" href="assets/css/scar-racing.css">
</head>
<body>

  <header class="header">
    <div class="container">

      <div class="tools-bar">
        <div class="language-switcher">
          Language:
          <a class="language-switcher__language  active">EN</a>
          <a class="language-switcher__language">FR</a>
        </div>
        <div class="mini-basket">
          <div class="mini-basket__items">2 items / 399.95€</div>
          <a class="mini-basket__checkout-btn  btn" href="">Checkout</a>
        </div>
      </div>

      <a class="site-logo" href="/"><img src="assets/img/site-logo.svg" alt="Scar Racing logo" height="28"></a>

      <nav class="nav">
        <ul>
          <li><a href="">Products</a></li>
          <li><a href="">Distributors</a></li>
          <li><a href="">Riders</a></li>
          <li><a href="">Insights</a></li>
        </ul>
        <a class="parts-finder-btn" href="" data-parts-finder-trigger>Parts finder</a>
      </nav>

    </div>
  </header>

  <section class="parts-finder" data-parts-finder>
    <div class="container">
      
      <form class="parts-finder-form">
        <fieldset>
          <div class="grid grid--auto grid--center">
            <div class="grid__cell">
              <label class="parts-finder-form__label" for="make">Make</label>
              <select class="parts-finder-form__select" name="make" id="make">
                <option value="null">- Select -</option>
                <option value="10">KTM</option>
                <option value="17">Honda</option>
                <option value="9">Yamaha</option>
                <option value="18">Kawasaki</option>
                <option value="19">Suzuki</option>
                <option value="13">Husaberg</option>
                <option value="14">Husqvarna</option>
                <option value="11">Gas Gas</option>
                <option value="12">Beta</option>
                <option value="21">Sherco</option>
                <option value="22">Aprilia</option>
            </select>
            </div>
            <div class="grid__cell">
              <label class="parts-finder-form__label" for="model">Model</label>
              <select class="parts-finder-form__select" name="model" id="model" disabled>
                <option value="null">- Select -</option>
                <option value="85 SX">85 SX</option>
                <option value="250 EXC-F">250 EXC-F</option>
                <option value="250 SX-F">250 SX-F</option>
                <option value="250 EXC">250 EXC</option>
                <option value="105 SX">105 SX</option>
                <option value="350 EXC-F">350 EXC-F</option>
                <option value="350 SX-F">350 SX-F</option>
                <option value="300 EXC">300 EXC</option>
                <option value="125 SX">125 SX</option>
                <option value="450 SMR">450 SMR</option>
                <option value="450 SX-F">450 SX-F</option>
                <option value="50 SX">50 SX</option>
                <option value="450 EXC">450 EXC</option>
                <option value="150 SX">150 SX</option>
                <option value="125 EXC">125 EXC</option>
                <option value="65 SX">65 SX</option>
                <option value="500 EXC">500 EXC</option>
                <option value="250 SX">250 SX</option>
                <option value="200 EXC">200 EXC</option>
              </select>
            </div>
            <div class="grid__cell">
              <label class="parts-finder-form__label" for="year">Year</label>
              <select class="parts-finder-form__select" name="year" id="year" disabled>
                <option value="null">- Select -</option>
                <option value="ALL">ALL</option>
              </select>
            </div>
          <button class="btn btn--hollow">Find parts</button>
        </fieldset>
      </form>

    </div>
  </section>
  