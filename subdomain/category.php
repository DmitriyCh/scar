<?php include('_includes/header.php'); ?>

  <nav class="breadcrumb  container">
    <ol class="breadcrumb__list">
      <li><a href="">Home</a></li>
      <li><a href="">Products</a></li>
      <li>Footpegs</li>
    </ol>
  </nav>

  <div class="container">
    <div class="grid grid--flush">
      <div class="1/4--lap-and-up grid__cell">
        <aside class="sidebar">
          <nav class="sidebar-nav">
            <h1 class="sidebar-nav__heading">Footpegs</h1>
            <select class="sidebar-nav__select">
              <option>Pivot Levers and levers for pitots and levers</option>
              <option>Triple Clamps</option>
              <option>Triple Clamps Accessories</option>
              <option>Handlebars and their assorted accessories</option>
              <option>Handlebars Accessories</option>
              <option>Parts &amp; Accessories</option>
              <option>Grips</option>
              <option>Tools</option>
              <option>Clearance Items</option>
            </select>
            <ul class="sidebar-nav__list">
              <li><a href="">Pivot Levers and levers for pitots and levers</a></li>
              <li><a href="">Triple Clamps</a></li>
              <li><a href="">Triple Clamps Accessories</a></li>
              <li><a href="">Handlebars and their assorted accessories</a></li>
              <li><a href="">Handlebars Accessories</a></li>
              <li><a href="">Parts &amp; Accessories</a></li>
              <li><a href="">Grips</a></li>
              <li><a href="">Tools</a></li>
              <li><a href="">Clearance Items</a></li>
            </ul>
          </nav>
        </aside>
      </div>
      <div class="3/4--lap-and-up grid__cell">
        <ul class="product-list  grid grid--tiny">
          <li class="product-list__product  1/2 1/3--handheld-and-up grid__cell">
            <a class="product-card" href="">
              <div class="product-card__img">
                <img src="assets/img/category/1.jpg" alt="Evolution footpegs">
              </div>
              <div class="product-card__details">
                <span class="product-card__title">Evolution footpegs, the evolution in footpegs</span>
                <span class="product-card__price">399.99€</span>
              </div>
            </a>
          </li>
          <li class="product-list__product  1/2 1/3--handheld-and-up grid__cell">
            <a class="product-card" href="">
              <div class="product-card__img">
                <img src="assets/img/category/2.jpg" alt="Standard footpegs">
              </div>
              <div class="product-card__details">
                <span class="product-card__title">Standard footpegs</span>
                <span class="product-card__price">399.99€</span>
              </div>
            </a>
          </li>
          <li class="product-list__product  1/2 1/3--handheld-and-up grid__cell">
            <a class="product-card" href="">
              <div class="product-card__img">
                <img src="assets/img/category/3.jpg" alt="Footpeg foam">
              </div>
              <div class="product-card__details">
                <span class="product-card__title">Footpeg foam</span>
                <span class="product-card__price">399.99€</span>
              </div>
            </a>
          </li>
          <li class="product-list__product  1/2 1/3--handheld-and-up grid__cell">
            <a class="product-card" href="">
              <div class="product-card__img">
                <img src="assets/img/category/4.jpg" alt="Evolution footpegs">
              </div>
              <div class="product-card__details">
                <span class="product-card__title">Evolution footpegs</span>
                <span class="product-card__price">399.99€</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

<?php include('_includes/footer.php'); ?>