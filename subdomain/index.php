<?php include('_includes/header.php'); ?>

  <section class="hero">
    <div class="hero__body  container">
      <h2 class="hero__headline">Scar handle-bars</h2>
      <h3 class="hero__subheading">Manufactured from aircraft aluminium, built to fly</h3>
      <img class="hero__product-shot" src="assets/img/hero-product.png" alt="Scar handlebars">
      <a class="hero__cta-btn  btn btn--full btn--arrow" href="">View handlebars</a>
    </div>
    <img class="hero__bg" src="assets/img/hero-bg.jpg" alt="Motocross rider">
  </section>

  <section class="home-teasers">
    <div class="container">
      <div class="grid grid--flush">
        <div class="1/2--handheld-and-up grid__cell">
          <article class="home-teaser -offset">
            <h3 class="home-teaser__heading">Shop Scar</h3>
            <img class="home-teaser__image" src="assets/img/home-teaser.png" alt="Bike parts">
            <a class="home-teaser__cta-btn  btn btn--mid btn--arrow" href="">Shop all products</a>
          </article>
        </div>
        <div class="1/2--handheld-and-up grid__cell">
          <article class="home-teaser">
            <h3 class="home-teaser__heading">Behind the scenes: Honda France</h3>
            <img class="home-teaser__image" src="assets/img/home-teaser2.jpg" alt="Motocross race">
            <a class="home-teaser__cta-btn  btn btn--mid btn--arrow" href="">Read more</a>
          </article>
        </div>
      </div>
    </div>
  </section>

<?php include('_includes/footer.php'); ?>