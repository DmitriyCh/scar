<script>
window.onload = function() {
	console.log('reload start');
	window.location.reload();
}
</script>
<?php

ini_set('memory_limit', '-1');

define('MAGENTO_ROOT', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);

$mageFilename = MAGENTO_ROOT . 'app' . DIRECTORY_SEPARATOR . 'Mage.php';

if (!file_exists($mageFilename)) {
    echo $mageFilename . ' was not found';
    exit;
}

require_once $mageFilename;
Mage::app();

function attributeValueExists($attCode, $attributeValue)
{
    $attribute = Mage::getModel('catalog/resource_eav_attribute')
        ->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attCode);

    $options   = $attribute->getSource()->getAllOptions();
    foreach ($options as $option) {
        if ($option['label'] == $attributeValue) {
            return true;
        }
    }
    return false;
}

function getAttributeValueId($attCode, $attributeValue)
{
    $attribute = Mage::getModel('catalog/resource_eav_attribute')
        ->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attCode);

    $options   = $attribute->getSource()->getAllOptions();
    foreach ($options as $option) {
        if ($option['label'] == $attributeValue) {
            return $option['value'];
        }
    }
    return false;
}

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "scar_old";

    // Create connections
    $conn = new mysqli($servername, $username, $password, $dbname);
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    
    // Check connection
    if ($conn->connect_error) {
        die("Connection to old db failed: " . $conn->connect_error);
    }
    
    //import categories
    // first copy all category images to meida/catalog/category
    $sql = "SELECT * FROM category ORDER BY catOrder ASC";
    $result = $conn->query($sql);
    Mage::app()->setCurrentStore(0);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $categories = Mage::getResourceModel('catalog/category_collection');
            $categories->addAttributeToSelect('url_key');
            $categories->addAttributeToFilter('url_key', $row['alias']);
            $existedCategory = $categories->getFirstItem();
            
            if($existedCategory && $existedCategory->getId()) {
                continue;
            }
            $parentId = '2';
            $category = Mage::getModel('catalog/category');
            $category->setName($row['catName']);
            $category->setUrlKey($row['alias']);
            $category->setIsActive(1);
            $category->setImage($row['catImage']);
            $category->setDisplayMode('PRODUCTS');
            $category->setIsAnchor(1); //for active anchor
            $category->setStoreId(0);
            $parentCategory = Mage::getModel('catalog/category')->load($parentId);
            $category->setPath($parentCategory->getPath());
            $category->save();
            $category = Mage::getModel('catalog/category')->load($category->getId())
                ->setStoreId(2)
                ->setName($row['catNameFr'])
                ->save()
                    ;
        }
    }
    
    //import products
    // first copy all images to meida/import
    $sql = "SELECT * FROM item inner join options ON item.itemID=options.optItem;";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $query = 'select entity_id from '.$resource->getTableName('catalog_product_entity').' where sku="'.$row['itemID'].'"';
            $productId = $readConnection->fetchOne($query);
            if($productId) {
                continue;
            }
            
            //assigned categories
            //get all images
            $sql = 'SELECT * FROM item_cat inner join category on category.catID = item_cat.catID where itemID = "'.$row['itemID'].'"';
            $catResult = $conn->query($sql);
            $catUrlKeys = array();
            if ($catResult->num_rows > 0) {
                while($catRow = $catResult->fetch_assoc()) {
                    $catUrlKeys[] = $catRow['alias'];
                }
            }
            $categoryCollection = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect('url_key')
                    ->addAttributeToFilter('url_key', array('in' => $catUrlKeys));
            $assignedCategories = $categoryCollection->getAllIds();
            
            //first check/create attrbiutes
            $optionAttributes = explode('|', $row['optTitle']);
            $optionAttributesIds = array();
            foreach ($optionAttributes as $attributePos => $optionAttribute) {
                $attributeCode = str_replace(' ','_',strtolower($optionAttribute));
                $query = 'select attribute_id from '.$resource->getTableName('eav_attribute').' where attribute_code="'.$attributeCode.'"';
                $attributeId = $readConnection->fetchOne($query);
                $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
                $installer->startSetup();
                if(!$attributeId) {
                    //create attribute
                    $attr = array (
                      'attribute_model' => NULL,
                      'backend' => '',
                      'type' => 'int',
                      'table' => '',
                      'frontend' => '',
                      'input' => 'select',
                      'label' => $optionAttribute,
                      'frontend_class' => '',
                      'source' => '',
                      'required' => '0',
                      'user_defined' => '1',
                      'default' => '',
                      'unique' => '0',
                      'note' => '',
                      'input_renderer' => NULL,
                      'global' => '1',
                      'visible' => '1',
                      'searchable' => '1',
                      'filterable' => '1',
                      'comparable' => '1',
                      'visible_on_front' => '1',
                      'is_html_allowed_on_front' => '1',
                      'is_used_for_price_rules' => '0',
                      'filterable_in_search' => '1',
                      'used_in_product_listing' => '0',
                      'used_for_sort_by' => '0',
                      'is_configurable' => '1',
                      'visible_in_advanced_search' => '1',
                      'wysiwyg_enabled' => '0',
                      'used_for_promo_rules' => '0',
                      'option' => 
                      array (
                        'values' => 
                        array (),
                      ),
                    );
                    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, $attr);
                    $query = 'select attribute_id from '.$resource->getTableName('eav_attribute').' where attribute_code="'.$attributeCode.'"';
                    $attributeId = $readConnection->fetchOne($query);
                }                
                $entityTypeId = Mage::getModel('catalog/product')
                        ->getResource()
                        ->getEntityType()
                        ->getId(); //product entity type
                $attributeSetId = $installer->getAttributeSetId($entityTypeId, 'Default');
                $allAttributesInSet = Mage::getModel('catalog/product_attribute_api')->items($attributeSetId);
                $addAttributeToSet = true;
                foreach($allAttributesInSet as $attributeInSet){
                    if($attributeInSet['attribute_id'] == $attributeId) {
                        $addAttributeToSet = false;
                        break;
                    }
                }
                if($addAttributeToSet) {
                    $installer->addAttributeToSet($entityTypeId, $attributeSetId, 'General', $attributeCode, 100);
                }
                $installer->endSetup();
                
                $optionAttributesIds[] = $attributeId;
                
                //check/add attribute options
                $newOptions = array();
                for($i = 1;$i <= 500; $i++) {
                    if($row['optValue'.$i] == '' || $row['optValue'.$i] == '##') {
                        continue;
                    }
                    $attrOptionsValues = explode('|', $row['optValue'.$i]);
                    $attrOptionsValue = '';
                    if(isset($attrOptionsValues[$attributePos]) && $attrOptionsValues[$attributePos] != '') {
                        $attrOptionsValue = $attrOptionsValues[$attributePos];
                    } else {
                        continue;
                    }
                    
                    if(attributeValueExists($attributeCode, $attrOptionsValue)) {
                        continue;
                    }
                    $newOptions[] = $attrOptionsValue;
                }
                
                if(!empty($newOptions)) {
                    $newOptions = array_unique($newOptions);
                    $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
                    $installer->startSetup();
                    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attributeCode);

                    if($attribute->getId() && $attribute->getFrontendInput()=='select') {
                        foreach($newOptions as $newOption)
                        {   
                            $option = array();
                            $arg_value = trim($newOption);
                            $option['attribute_id'] = $attribute->getId();
                            $option['value'] = array(array($newOption));//$newOptions;
                            $installer->addAttributeOption($option);
                        }
                    }
                    $installer->endSetup();
                }
            }
            
            //create simple products
            $configurableProductsData = array();
            for($i = 1;$i <= 500; $i++) {
                if($row['optValue'.$i] == '' || $row['optValue'.$i] == '##') {
                    continue;
                }
                
                $attrOptionsValues = explode('|', $row['optValue'.$i]);
                $setData = array();
                $configurableProductData = array();
                foreach($attrOptionsValues as $pos => $optionValue) {
                    if(isset($optionAttributes[$pos])) {
                        $attributeCode = str_replace(' ','_',strtolower($optionAttributes[$pos]));
                        $query = 'select attribute_id from '.$resource->getTableName('eav_attribute').' where attribute_code="'.$attributeCode.'"';
                        $attributeId = $readConnection->fetchOne($query);
                        $optionValueId = getAttributeValueId($attributeCode, $optionValue);
                        $setData[$attributeCode] = $optionValueId;
                        $configurableProductData[] = array(
                            'label' => $optionValue, //attribute label
                            'attribute_id' => $attributeId, //attribute ID of attribute 'color' in my store
                            'value_index' => $optionValueId, //value of 'Green' index of the attribute 'color'
                            'is_percent' => '0', //fixed/percent price for this option
                            'pricing_value' => '0' //value for the pricing
                        );
                    }
                }
                
                $simpleProduct = Mage::getModel('catalog/product');
                $simpleProduct
                    ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId(4) //ID of a attribute set named 'default'
                    ->setTypeId('simple') //product type
                    ->setCreatedAt(strtotime('now')) //product creation time
                //    ->setUpdatedAt(strtotime('now')) //product update time
                    ->setSku($row['itemID'].'-'.$i) //SKU
                    ->setName($row['itemName']) //product name
                    //->setWeight(4.0000)
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                    //->setManufacturer(28) //manufacturer id
                    /*->setNewsFromDate('06/26/2014') //product set as new from
                    ->setNewsToDate('06/30/2014') //product set as new to
                    ->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)*/
                    ->setPrice($row['itemPrice']) //price in form 11.22
                    /*->setCost(22.33) //price in form 11.22
                    ->setSpecialPrice(00.44) //special price in form 11.22
                    ->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
                    ->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
                    ->setMsrpEnabled(1) //enable MAP
                    ->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
                    ->setMsrp(99.99) //Manufacturer's Suggested Retail Price*/
                    ->setMetaTitle($row['itemName'])
                    ->setMetaKeyword($row['itemName'])
                    //->setMetaDescription('test meta description 2')
                    ->setDescription($row['itemText'])
                    ->setShortDescription($row['itemText'])
                    ->setMediaGallery(array('images' => array(), 'values' => array())) //media gallery initialization
                    ->setCategoryIds($assignedCategories)
                    ->save()
                    ;
                    //->setCategoryIds(array(3, 10)); //assign product to categories
                $simpleProduct = Mage::getModel('catalog/product')->load($simpleProduct->getId());
                $simpleProduct->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock' => 0, //manage stock
                        /*'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                        'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart*/
                        'is_in_stock' => 1, //Stock Availability
                        //'qty' => 999 //qty
                    )
                    )->addData($setData)->save();
                
                $configurableProductsData[$simpleProduct->getId()] = $configurableProductData;
            }
            
            $configProduct = Mage::getModel('catalog/product');
            $configProduct
                //    ->setStoreId(1) //you can set data in store scope
                ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                ->setAttributeSetId(4) //ID of a attribute set named 'default'
                ->setTypeId('configurable') //product type
                ->setCreatedAt(strtotime('now')) //product creation time
                //    ->setUpdatedAt(strtotime('now')) //product update time
                ->setSku($row['itemID']) //SKU
                ->setUrlKey($row['alias'])
                ->setName($row['itemName']) //product name
                ->setWeight(0.0000)
                ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                //->setManufacturer(28) //manufacturer id
                //->setNewsFromDate('06/26/2014') //product set as new from
                // ->setNewsToDate('06/30/2014') //product set as new to
                //->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)
                ->setPrice($row['itemPrice']) //price in form 11.22
                //->setMsrpEnabled(1) //enable MAP
                //->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
                //->setMsrp(99.99) //Manufacturer's Suggested Retail Price
                ->setMetaTitle($row['itemName'])
                ->setMetaKeyword($row['itemName'])
                //->setMetaDescription($row['itemName'])
                ->setDescription($row['itemText'])
                ->setShortDescription($row['itemText'])
                ->setCategoryIds($assignedCategories)
                ->setMediaGallery(array('images' => array(), 'values' => array())) //media gallery initialization
                ->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock' => 0, //manage stock
                        'is_in_stock' => 1, //Stock Availability
                    )
                )
                ;
            $configProduct->getTypeInstance()->setUsedProductAttributeIds($optionAttributesIds); //attribute ID of attribute 'color' in my store
            $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();

            $configProduct->setCanSaveConfigurableAttributes(true);
            $configProduct->setConfigurableAttributesData($configurableAttributesData);
            $configProduct->setConfigurableProductsData($configurableProductsData);
            $configProduct->save();
            
            $configProduct = Mage::getModel('catalog/product')->load($configProduct->getId());
            //get all images
            $sql = 'SELECT * FROM image where imgItem="'.$row['itemID'].'" ORDER BY imgMaster DESC';
            $imgResult = $conn->query($sql);

            if ($imgResult->num_rows > 0) {
                while($imgRow = $imgResult->fetch_assoc()) {
                    $filePath = Mage::getBaseDir('media') . DS . 'import' . DS . $imgRow['imgFile'];
                    $configProduct->addImageToMediaGallery($filePath, null, false, false);
                }
            }
            
            $mediaGallery = $configProduct->getMediaGallery();
            //if there are images
            if (isset($mediaGallery['images'])){
                //loop through the images
                foreach ($mediaGallery['images'] as $image){
                    //set the first image as the base image
                    Mage::getSingleton('catalog/product_action')->updateAttributes(array($configProduct->getId()), array('image'=>$image['file']), 0);
                    Mage::getSingleton('catalog/product_action')->updateAttributes(array($configProduct->getId()), array('small_image'=>$image['file']), 0);
                    Mage::getSingleton('catalog/product_action')->updateAttributes(array($configProduct->getId()), array('thumbnail'=>$image['file']), 0);
                    //stop
                    break;
                }
            }
            
            $configProduct->setStoreId(2) //you can set data in store scope
                ->setName(utf8_encode($row['itemNameFr'])) //product name
                ->setDescription(utf8_encode($row['itemTextFr']))
                ->setShortDescription(utf8_encode($row['itemTextFr']))
                ->save();
            
        }
    }
    
    $conn->close();
    
    echo 'import ended successfully';