<?php

ini_set('max_execution_time', -1);

ini_set('memory_limit', -1);

require_once 'abstract.php';


class Shell_Fix_Attribute_Color extends Mage_Shell_Abstract
{
    /**
     * Log Enable or Disable
     * 
     * @var boolean
     */
    protected $_logEnable = true;
    
    /**
     * Product collection
     * 
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_productCollection;
    
    /**
     * Product Atribute code for color
     * 
     * @var string
     */
    protected $_attributeCode = 'color';


    /**
     * Product Atribute
     * 
     * @var Mage_Catalog_Model_Resource_Eav_Attribute 
     */
    protected $_attribute;

    /**
     * Product Atribute Options
     * 
     * @var array
     */
    protected $_attributeOptions;
    
    /**
     * Right Product Atribute Options
     * 
     * @var array
     */
    protected $_rightAttributeOptions;
    
    /**
     * Delete Product Atribute Options
     * 
     * @var array
     */
    protected $_deleteAttributeOptions;

    /**
     * Get Product Collection
     * 
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        if(!$this->_productCollection){
            $this->_productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        }
        return $this->_productCollection;
    }
    
    /**
     * Start Fix Attribute Color
     * 
     */
    public function run()
    {
        $this->log('Start Fix Attribute Color' . date("Y-m-d H:i:s"));
        
        try{
            $this->fixAttributeOptions();
        }
        catch (Exception $e) {
            $this->logException($e);
        }
        
        $this->log('End Fix Attribute Color' . date("Y-m-d H:i:s"));
    }
    
    /**
     * Set correct collor option for product
     * 
     */
    public function fixAttributeOptions()
    {
        $collection = $this->getProductCollection();
        foreach($collection as $product){
            $color = $product->getColor();
            if(!empty($color)){
                $label = $this->getOptionLabel($color);
                $newOptionId = $this->getCorrectOptionId($label);
                if(!empty($newOptionId)){
                    $product->setColor($newOptionId);
                    $product->save();
                    $logData = array(
                        'ProductId' => $product->getId(),
                        'Color Label' => $label,
                        'Old Option' => $color,
                        'New Option' => $newOptionId,
                    );
                    $this->log($logData);
                }
            }
        }
        $this->deleteAttributeOptions();
    }

    /**
     * Delete not correct options
     * 
     */
    public function deleteAttributeOptions()
    {
        if(!$this->_deleteAttributeOptions){
            $this->filterRightOptions();
        }
        
        $optionsDelete = array();
        foreach($this->_deleteAttributeOptions as $option){
            $optionsDelete['delete'][$option['value']] = true;
            $optionsDelete['value'][$option['value']] = true;
        }
        
        $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
        $installer->addAttributeOption($optionsDelete);
        $this->log('Delete Options');
        $this->log($this->_deleteAttributeOptions);
    }

    /**
     * Get Correct OptionId
     * 
     * @param string $label
     * @return int|null
     */
    public function getCorrectOptionId($label)
    {
        if(!$this->_rightAttributeOptions){
            $this->filterRightOptions();
        }
        
        foreach($this->_rightAttributeOptions as $option){
            if($option['label'] == $label){
                return $option['value'];
            }
        }
        return null;
    }


    /**
     * Get Attibute Option Label
     * 
     * @param int $valueId
     * @return string | null
     */
    public function getOptionLabel($valueId)
    {
        if(!empty($valueId)){
            $options = $this->getAttributeOptions();
            foreach($options as $option){
                if($option['value'] == $valueId){
                    return $option['label'];
                }
            }
        }
        return null;
    }


    /**
     * Divide attribute options on correct and for delete
     * 
     */
    public function filterRightOptions()
    {
        $options = $this->getAttributeOptions();
        
        foreach($options as $option){
            if(!isset($this->_rightAttributeOptions[$option['label']])){
                $this->_rightAttributeOptions[$option['label']] = $option;
            }
            else{
                $this->_deleteAttributeOptions[] = $option;
            }
        }
    }

        /**
     * Get attribute by code
     * 
     * @return Mage_Catalog_Model_Resource_Eav_Attribute
     */
    public function getAttribute()
    {
        if(!$this->_attribute){
            $this->_attribute = Mage::getSingleton('eav/config')
                    ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $this->_attributeCode);
        }
        return $this->_attribute;
    }
    
    /**
     * Get Attribute Options
     * 
     * @return array
     */
    public function getAttributeOptions()
    {
        if(!$this->_attribute){
            $this->_attribute = $this->getAttribute();
        }
        
        if($this->_attribute->usesSource()) {
            $this->_attributeOptions = $this->_attribute->getSource()->getAllOptions(false);
        }
        
        return $this->_attributeOptions;
    }
    
    /**
     * Write Log
     * 
     * @param array $data
     */
    public function log($data)
    {
        if($this->_logEnable){
            Mage::log(print_r($data, true), null, 'fix_attribute_color.log', true);
        }
    }
    
    /**
     * Write exception to log
     *
     * @param Exception $e
     */
    public function logException(Exception $e)
    {
        Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'fix_attribute_color.log', true);
    }
}

$shell = new Shell_Fix_Attribute_Color();
$shell->run();