<?php

class BroSolutions_CustomSearch_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isCustomSearchEnabled() {
        if(Mage::registry('search_make') || Mage::registry('search_model') || Mage::registry('search_year')) {
            return true;
        }
        return false;
    }
    
    public function getModelSearchOptions($makeVal = null) {
        $html = '<option value>- '.Mage::helper('customsearch')->__('Select').' -</option>';
        if($makeVal) {
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('model')
                    ->addAttributeToFilter('make',$makeVal);

            $resultArray = array();
            foreach ($productCollection as $product) {
                $resultArray[$product->getModel()] = $product->getAttributeText('model');
            }

            $currentModel = 0;
            if(Mage::registry('search_model')) {
                $currentModel = Mage::registry('search_model');
            }
            $resultArray = array_unique($resultArray);
            $html .= $this->generateHtmlOptions($currentModel, $resultArray);
            
        }
        
        return $html;
    }
    
    public function generateHtmlOptions($currentKey, $resultArray) {
        $html = '';
        foreach ($resultArray as $key=>$value) {
            if($key == $currentKey) {
                $html .= '<option value="'.$key.'" selected="selected">'.$value.'</option>';
            } else {
                $html .= '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        return $html;
    }


    public function getYearSearchOptions($makeVal = null, $modelVal = null) {
        $html = '<option value>- '.Mage::helper('customsearch')->__('Select').' -</option>';
        if($makeVal && $modelVal) {
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('year')
                    ->addAttributeToFilter('model',$modelVal)
                    ->addAttributeToFilter('make',$makeVal);

            $resultArray = array();
            foreach ($productCollection as $product) {
                $resultArray[$product->getYear()] = $product->getAttributeText('year');
            }

            $resultArray = array_unique($resultArray);
            $currentYear = 0;
            if(Mage::registry('search_year')) {
                $currentYear = Mage::registry('search_year');
            }
            $html .= $this->generateHtmlOptions($currentYear, $resultArray);
        }
        
        return $html;
    }
}
