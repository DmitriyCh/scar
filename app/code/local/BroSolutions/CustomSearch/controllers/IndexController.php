<?php

class BroSolutions_CustomSearch_IndexController extends Mage_Core_Controller_Front_Action
{
    public function updatemodelAction() {
        $data = $this->getRequest()->getParams();
        if(!isset($data['makeVal'])) {
            return false;
        }
        
        $html = Mage::helper('customsearch')->getModelSearchOptions($data['makeVal']);
        
        $this->getResponse()->setBody($html);
    }
    
    public function updateyearAction() {
        $data = $this->getRequest()->getParams();
        if(!isset($data['modelVal']) || !isset($data['makeVal'])) {
            return false;
        }
        
        $html = Mage::helper('customsearch')->getYearSearchOptions($data['makeVal'], $data['modelVal']);
        
        $this->getResponse()->setBody($html);
    }
    
    public function resultAction() {
        $data = $this->getRequest()->getParams();
        if(isset($data['make'])) {
            Mage::register('search_make', $data['make']);
        }
        if(isset($data['model'])) {
            Mage::register('search_model', $data['model']);
        }
        if(isset($data['year'])) {
            Mage::register('search_year', $data['year']);
        }
        $this->loadLayout();
        $this->renderLayout(); 
    }
}