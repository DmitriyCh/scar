<?php

class BroSolutions_CustomSearch_Block_Product_List extends Mage_Catalog_Block_Product_List
{
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
//            $layer = $this->getLayer();
//            /* @var $layer Mage_Catalog_Model_Layer */
//            if ($this->getShowRootCategory()) {
//                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
//            }
//
//            // if this is a product view page
//            if (Mage::registry('product')) {
//                // get collection of categories this product is associated with
//                $categories = Mage::registry('product')->getCategoryCollection()
//                    ->setPage(1, 1)
//                    ->load();
//                // if the product is associated with any category
//                if ($categories->count()) {
//                    // show products from this category
//                    $this->setCategoryId(current($categories->getIterator()));
//                }
//            }
//
//            $origCategory = null;
//            if ($this->getCategoryId()) {
//                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
//                if ($category->getId()) {
//                    $origCategory = $layer->getCurrentCategory();
//                    $layer->setCurrentCategory($category);
//                    $this->addModelTags($category);
//                }
//            }
//            $this->_productCollection = $layer->getProductCollection();
//
//            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());
//
//            if ($origCategory) {
//                $layer->setCurrentCategory($origCategory);
//            }
            
            $productCollection = Mage::getModel('catalog/product')->getCollection();//->addAttributeToSelect('*');
            if(Mage::registry('search_make')) {
                $productCollection->addAttributeToFilter('make',Mage::registry('search_make'));
            }
            if(Mage::registry('search_model')) {
                $productCollection->addAttributeToFilter('model',Mage::registry('search_model'));
            }
            if(Mage::registry('search_year')) {
                $productCollection->addAttributeToFilter('year',Mage::registry('search_year'));
            }
            $resultProductIds = array();
            foreach ($productCollection as $product) {
                if($product->getTypeId() == 'simple' ) {
                    $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                          ->getParentIdsByChild($product->getId());
                    foreach ($parentIds as $parentId) {
                        $resultProductIds[] = $parentId;
                    }
                } else {
                    $resultProductIds[] = $product->getId();
                }
            }
            
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in' => $resultProductIds));
            $this->_productCollection = $productCollection;
        }
        
        return $this->_productCollection;
    }

    public function getSearchResultString()
    {
        $post = $this->getRequest()->getPost();
        $attrColl = Mage::getSingleton("eav/config");

        $string = array();
        foreach ($post as $attr => $value) {
            //$option = Mage::helper("customsearch")->__($attrColl->getAttribute('catalog_product', $attr)->getSource()->getOptionText($value));
            //$string[] = "{$attr}: {$option}";
            $string[] = Mage::helper("customsearch")->__($attrColl->getAttribute('catalog_product', $attr)->getSource()->getOptionText($value));
        }

        return implode(' ', $string);
    }
}
