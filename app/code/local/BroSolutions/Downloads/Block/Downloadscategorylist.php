<?php

class BroSolutions_Downloads_Block_Downloadscategorylist extends Mage_Core_Block_Template
{
    /**
     * Download Category
     * 
     * @var BroSolutions_Downloads_Model_Downloadscategory
     */
    protected $_category;
    
    /**
     * Get Downloads Category collection
     * 
     * @return BroSolutions_Downloads_Model_Resource_Downloadscategory_Collection
     */
    public function getCategoryList()
    {
        $collection = Mage::getModel('downloads/downloadscategory')->getCollection();
        $collection->addFieldToFilter('status' , array("eq" => 1));

        return $collection;
    }
    
    /**
     * Get Download Category
     * 
     * @return BroSolutions_Downloads_Model_Resource_Downloadscategory_Collection
     */
    public function getCategory()
    {
        if(!$this->_category){
            $id = Mage::app()->getRequest()->getParam('id');
            $this->_category = Mage::getModel('downloads/downloadscategory')->load($id);
        }
        return $this->_category;
    }
    
    public function _prepareLayout()
    {
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
         
        $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('downloads')->__('Home'), 
                    'title' => Mage::helper('downloads')->__('Home Page'), 
                    'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
                )
        );
        
        $breadcrumbs->addCrumb('downloads', array(
                    'label' => Mage::helper('downloads')->__('Downloads'), 
                    'title' => Mage::helper('downloads')->__('Downloads'), 
                    //'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'downloads',
                )
        );
        
        parent::_prepareLayout();
    }
}
