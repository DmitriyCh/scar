<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadscategory_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('downloads');
        $categoryModel = Mage::registry('current_downloadscategory');
        
        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));
                
        $fieldset->addField('name', 'text', array(
            'label'    => $helper->__('Name'),
            'required' => true,
            'name'     => 'name',
        ));
        
        $fieldset->addField('image', 'image', array(
            'name' => 'image',
            'label' => $helper->__('Image'),
            'title' => $helper->__('Image'),
            'required' => false,
        ));
        
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => '1',
                    'label' => $helper->__('Enabled'),
                ),
                array(
                    'value' => '0',
                    'label' => $helper->__('Disabled'),
                ),
            ),
            'required' => true,
        ));
        
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('description', 'editor', array(
            'label'    => $helper->__('Description'),
            'title'    => $helper->__('Description'),
            'style'    => 'height:300px; width: 550px;',
            'required' => false,
            'name'     => 'description',
            'wysiwyg'  => true,
            'config'   => $wysiwygConfig,
        ));
        
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        }
        else {
            $form->setValues($categoryModel->getData());
        }
        
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
