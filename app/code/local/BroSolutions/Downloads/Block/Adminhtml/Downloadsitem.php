<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadsitem extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Internal constructor
     *
     */
    public function __construct()
    {
        $helper = Mage::helper('downloads');
        $this->_blockGroup = 'downloads';
        $this->_controller = 'adminhtml_downloadsitem';

        $this->_headerText = $helper->__('Items Management');
        $this->_addButtonLabel = $helper->__('Add New Item');
        
        parent::__construct();
    }
}