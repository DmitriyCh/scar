<?php

class BroSolutions_Downloads_Block_Adminhtml_Renderer_Grid_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    
    /**
     * 
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row) {
        $imageId = $row->getId();
        $imageName = $row->getImage();
        
        if(!$imageName) 
            return Mage::helper('downloads')->__('No image');
        
        $imageUrl = Mage::helper('downloads')->getImageUrl($imageName);
        
        return  '<img id="tolltip_image_' . $imageId . '" src="' . $imageUrl . '" width="70px" height="70px"/>'.
                '<script type="text/javascript">
                    new Tooltip("tolltip_image_'.$imageId.'", "'.$imageUrl.'");
                    $$(".tooltip img").each(function(item){
                        item.style.width="300px";
                        item.style.height="300px";
                    });
                </script>'
                
        ;
    }

}
