<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadscategory extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Internal constructor
     *
     */
    public function __construct()
    {
        $helper = Mage::helper('downloads');
        $this->_blockGroup = 'downloads';
        $this->_controller = 'adminhtml_downloadscategory';

        $this->_headerText = $helper->__('Categories Management');
        $this->_addButtonLabel = $helper->__('Add New Category');
        
        parent::__construct();
    }
}