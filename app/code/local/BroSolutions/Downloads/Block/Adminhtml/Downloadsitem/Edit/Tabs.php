<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadsitem_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('downloads');
        $this->setId('downloadsitem_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Item Information'));
    }
    
    /**
     * Preparing layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('downloads');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('downloads/adminhtml_downloadsitem_edit_tabs_general')->toHtml(),
        ));
        
        $this->addTab('item_files_section', array(
            'label' => $helper->__('Item Files'),
            'title' => $helper->__('Item Files'),
            'content' => $this->getLayout()->createBlock('downloads/adminhtml_downloadsitem_edit_tabs_files')->toHtml(),
        ));
        
        return parent::_prepareLayout();
    }
}
