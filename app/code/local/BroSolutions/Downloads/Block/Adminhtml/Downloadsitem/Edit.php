<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadsitem_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('downloads');
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'downloads';
        $this->_controller = 'adminhtml_downloadsitem';
        
        $this->_updateButton('save', 'label', $helper->__('Save Item'));
        $this->_updateButton('delete', 'label', $helper->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => $helper->__('Save Item And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -10);

        
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * Get Header Text
     * 
     * @return string
     */
    public function getHeaderText()
    {
        $helper = Mage::helper('downloads');
        $categoryModel = Mage::registry('current_downloadsitem');

        if ($categoryModel->getId()) {
            return $helper->__("Edit Item ID '%s'", $categoryModel->getId());
        } else {
            return $helper->__("Add New Item");
        }
    }
    
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
}
