<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadsitem_Edit_Tabs_Files extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('downloads');
        $itemModel = Mage::registry('current_downloadsitem');
        
        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('item_files_form', array(
            'legend' => $helper->__('Item Files')
        ));
        
        
        $fieldset->addField('files', 'textarea', array(
            'label'    => $helper->__('Files'),
            'title'    => $helper->__('Files'),
            'name'     => 'files',
            'required' => false,
        ));
        
        $filesOptions = $form->getElement('files');

        $filesOptions->setRenderer(
            $this->getLayout()->createBlock('downloads/adminhtml_downloadsitem_edit_tabs_renderer_files')
        );
        
        
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        }
        else {
            $form->setValues($itemModel->getData());
        }
        
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
