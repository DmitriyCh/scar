<?php

class BroSolutions_Downloads_Block_Adminhtml_Downloadsitem_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('downloadsitemGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Prepare grid collection object
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('downloads/downloadsitem')->getCollection();
        
        $collection->getSelect()->join(
                array('bro_downloads_category' => 'bro_downloads_category'),
                'main_table.category_id = bro_downloads_category.entity_id',
                array('category_name' => 'bro_downloads_category.name')
        );
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * Prepare Grid Columns
     * 
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('downloads');
        
        $this->addColumn('entity_id', array(
            'header'    => $helper->__('Item ID'),
            'index'     => 'entity_id',
            'align'     => 'right',
            'width'     => '50px',
        ));
        
        $this->addColumn('image', array(
            'header'    => $helper->__('Image'),
            'index'     => 'image',
            'align'     => 'center',
            'width'     => '70px',
            'renderer' => 'downloads/adminhtml_renderer_grid_image'
        ));
        
        $this->addColumn('name', array(
            'header'    => $helper->__('Name'),
            'index'     => 'name',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('category_name', array(
            'header'    => $helper->__('Category'),
            'index'     => 'category_name',
            'type'      => 'text',
            'width'     => '100px',
            'align'     => 'left',
        ));
        
        $this->addColumn('status', array(
            'header'    => $helper->__('Status'),
            'index'     => 'status',
            'align'     => 'left',
            'width'     => '100px',
            'type'      => 'options',
            'options'   => array(
                '1' => 'Enabled',
                '0' => 'Disabled',
            ),
        ));
        
        $this->addColumn('created_at', array(
            'header'    => $helper->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('updated_at', array(
            'header'    => $helper->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('action', array(
            'header'    =>  $helper->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => array(
                array(
                    'caption'   => $helper->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'id'
                ),
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
            'align'     => 'center',
        ));
        
        return parent::_prepareColumns();
    }
    
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addJs('brosolutions/tooltip.js');
        parent::_prepareLayout();
    }
    
    /**
     * Get Row Url
     * 
     * @param BroSolutions_Downloads_Model_Downloadscategory $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    /**
     * Prepare Massaction
     * 
     * @return BroSolutions_Downloads_Block_Adminhtml_Downloadscategory_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('downloadsitem');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
