<?php

class BroSolutions_Downloads_Block_Downloadsitemlist extends Mage_Core_Block_Template
{
    /**
     * Download Category
     * 
     * @var BroSolutions_Downloads_Model_Downloadscategory
     */
    protected $_category;
    /**
     * Get Downloads category items
     * 
     * @return BroSolutions_Downloads_Model_Resource_Downloadsitem_Collection
     */
    public function getCategoryItems()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        $collection = Mage::getModel('downloads/downloadsitem')->getCollection();
        $collection->addFieldToFilter('status' , array("eq" => 1));
        
        if(!empty($id)){
            $collection->addFieldToFilter('category_id' , array("eq" => $id));
        }
        
        return $collection;
    }
    
    /**
     * Get Download Category
     * 
     * @return BroSolutions_Downloads_Model_Resource_Downloadscategory_Collection
     */
    public function getCategory()
    {
        if(!$this->_category){
            $id = Mage::app()->getRequest()->getParam('id');
            $this->_category = Mage::getModel('downloads/downloadscategory')->load($id);
        }
        return $this->_category;
    }

    public function _prepareLayout()
    {
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
         
        $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('downloads')->__('Home'), 
                    'title' => Mage::helper('downloads')->__('Home Page'), 
                    'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
                )
        );
        
        $breadcrumbs->addCrumb('downloads', array(
                    'label' => Mage::helper('downloads')->__('Downloads'), 
                    'title' => Mage::helper('downloads')->__('Downloads'), 
                    'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'downloads',
                )
        );
        
        $id = Mage::app()->getRequest()->getParam('id');
        if(!empty($id)){
            $category = $this->getCategory();
            $breadcrumbs->addCrumb('category', array(
                    'label' => $category->getName(), 
                    'title' => $category->getName(), 
                    //'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'downloads/index/category/id/' . $category->getId(),
                )
        );
        }
        
        parent::_prepareLayout();
    }
}
