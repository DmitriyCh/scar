<?php

class BroSolutions_Downloads_Model_Downloadsitem extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('downloads/downloadsitem');
    }
}