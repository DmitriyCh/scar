<?php

class BroSolutions_Downloads_Model_Resource_Downloadscategory_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    /**
     * Initialization here
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('downloads/downloadscategory');
    }
}
