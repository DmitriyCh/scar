<?php

class BroSolutions_Downloads_Model_Resource_Downloadsitem extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        $this->_init('downloads/table_downloads_items', 'entity_id');
    }
}
