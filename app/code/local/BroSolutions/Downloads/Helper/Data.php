<?php

class BroSolutions_Downloads_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Upload Image
     * 
     * @param array $file || $_FILES['image']
     * @return string
     */
    public function uploadImage($file, $path = '')
    {
        if(!empty($path)){
            $path = 'downloads' . '/' . $path;
        }
        else{
            $path = 'downloads';
        }
        
        $media_path = Mage::getBaseDir('media') . '/' . $path;
        try {
            $uploader = new Varien_File_Uploader($file);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            
            /**
             * Set the file upload mode
             * false -> get the file directly in the specified folder
             * true -> get the file in the product like folders
             * (file.jpg will go in something like /media/f/i/file.jpg)
             */
            $uploader->setFilesDispersion(false);
            
            
            $imgFilename = strtolower(str_replace(' ', '_', $file['name']));
            if(is_file($this->getImagePath($path . '/' . $imgFilename))){
                $arrName = explode('.', $file['name']);
                $ext = array_pop($arrName);
                $fileName = implode('.', $arrName). '_' . date('Y_m_d_H_i_s') . '.' . $ext;
                $imgFilename = strtolower(str_replace(' ', '_', $fileName));
            }
            
            $result = $uploader->save($media_path, $imgFilename);
            $resultImage = $path . '/' . ltrim($result['file'], '/');
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving item image.'));
        }
        
        return $resultImage;
    }
    
    /**
     * Upload Image
     * 
     * @param array $file || $_FILES['file']
     * @return string
     */
    public function uploadFile($file, $path = '')
    {
        if(!empty($path)){
            $path = 'downloads' . '/' . $path;
        }
        else{
            $path = 'downloads';
        }
        
        $media_path = Mage::getBaseDir('media') . '/' . $path;
        try {
            $uploader = new Varien_File_Uploader($file);
            $uploader->setAllowRenameFiles(false);
            
            /**
             * Set the file upload mode
             * false -> get the file directly in the specified folder
             * true -> get the file in the product like folders
             * (file.jpg will go in something like /media/f/i/file.jpg)
             */
            $uploader->setFilesDispersion(false);
            
            
            $uploadFilename = strtolower(str_replace(' ', '_', $file['name']));
            if(is_file($this->getImagePath($path . '/' . $uploadFilename))){
                $arrName = explode('.', $file['name']);
                $ext = array_pop($arrName);
                $fileName = implode('.', $arrName). '_' . date('Y_m_d_H_i_s') . '.' . $ext;
                $uploadFilename = strtolower(str_replace(' ', '_', $fileName));
            }
            
            $result = $uploader->save($media_path, $uploadFilename);
            $resultFile = $path . '/' . ltrim($result['file'], '/');
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving '.$file['name'].' file.'));
        }
        
        return $resultFile;
    }
    
    /**
     * Get Image Path
     * 
     * @param string banner Name
     * @return string Image Path
     */
    public function getImagePath($fileName = null)
    {
        $path = Mage::getBaseDir('media');
        if ($fileName) {
            return $path . DS . $fileName;
        } else {
            return $path;
        }
    }
    
    /**
     * Delete image
     * 
     * @param string $imagePath
     * @return boolean
     */
    public function deleteImage($imagePath)
    {
        $imagePath = Mage::getBaseDir("media") . DS . str_replace("/", DS, $imagePath);
        
        if (is_file($imagePath)) {
            if (unlink($imagePath)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get Image Url
     * 
     * @param string Image Name
     * @return string image url
     */
    public function getImageUrl($fileName = null)
    {
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        if ($fileName) {
            return $url . $fileName;
        } else {
            return $url;
        }
    }
}
