<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `{$installer->getTable('downloads/table_downloads_items')}` 
        ADD `files` TEXT NOT NULL AFTER `description`;
");

$installer->endSetup();

