<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableDownloadsCategoryName = $installer->getTable('downloads/table_downloads_category');
$installer->getConnection()->dropTable($tableDownloadsCategoryName);

$tableDownloadsCategory = $installer->getConnection()
        ->newTable($tableDownloadsCategoryName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'auto_increment' => true,
            'unsigned' => true,
            'nullable'=> false,
            'primary' => true,
        ), 'Entity ID')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Name')
        ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Category Image')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Description')
        ->addColumn('status',  Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'unsigned' => true,
            'nullable'=> false,
            'default' => true
        ), 'Status')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Updated At');

$tableDownloadsCategory->setOption('type', 'InnoDB');
$tableDownloadsCategory->setOption('charset', 'utf8');
$installer->getConnection()->createTable($tableDownloadsCategory);

/* ------------ Create Table bro_downloads_items ------------*/

$tableDownloadsItemName = $installer->getTable('downloads/table_downloads_items');
$installer->getConnection()->dropTable($tableDownloadsItemName);

$tableDownloadsItem = $installer->getConnection()
        ->newTable($tableDownloadsItemName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'auto_increment' => true,
            'unsigned' => true,
            'nullable'=> false,
            'primary' => true,
        ), 'Entity ID')
        ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'unsigned' => true,
            'nullable'=> false,
        ), 'Category ID')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Name')
        ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Item Image')

        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Description')
        ->addColumn('status',  Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'unsigned' => true,
            'nullable'=> false,
            'default' => true
        ), 'Status')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Updated At');

$tableDownloadsItem->setOption('type', 'InnoDB');
$tableDownloadsItem->setOption('charset', 'utf8');
$installer->getConnection()->createTable($tableDownloadsItem);

$installer->endSetup();

