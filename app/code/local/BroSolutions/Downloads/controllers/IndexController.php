<?php

class BroSolutions_Downloads_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index Action
     * 
     */
    public function indexAction() 
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * Category Action
     * 
     */
    public function categoryAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}