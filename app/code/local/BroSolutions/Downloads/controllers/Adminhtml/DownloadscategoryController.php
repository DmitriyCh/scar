<?php

class BroSolutions_Downloads_Adminhtml_DownloadscategoryController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('downloads');
    }
    
    /**
     * Index Action
     * 
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('downloads');
        $contentBlock = $this->getLayout()->createBlock('downloads/adminhtml_downloadscategory');
        $this->_addContent($contentBlock);
        $this->renderLayout();
    }
    
    /**
     * New Download Action
     * 
     */
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    /**
     * Edit Download Category Action
     * 
     */
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_downloadscategory', Mage::getModel('downloads/downloadscategory')->load($id));
        
        $this->loadLayout();
        $this->_setActiveMenu('downloads');
        
        $leftBlock = $this->getLayout()->createBlock('downloads/adminhtml_downloadscategory_edit_tabs');
        $this->_addLeft($leftBlock);
        
        $contentBlock = $this->getLayout()->createBlock('downloads/adminhtml_downloadscategory_edit');
        $this->_addContent($contentBlock);
        
        $this->renderLayout();
    }
        
    /**
     * Save Download Item Action
     * 
     * @return void
     */
    public function saveAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('downloads');
                $categoryModel = Mage::getModel('downloads/downloadscategory');

                if(!empty($id) && isset($data['image']['delete']) && $data['image']['delete'] == 1) {
                    $imagePath = $categoryModel->load($id)->getImage();
                    $helper->deleteImage($imagePath);
                    $data['image'] = '';
                }
                
                if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
                    if(!empty($id)){
                        $imagePath = $categoryModel->load($id)->getImage();
                        $helper->deleteImage($imagePath);
                    }
                    
                    $data['image'] = $helper->uploadImage($_FILES['image'], 'category');
                }
                else{
                    if(isset($data['image']['value'])){
                        $data['image'] = $data['image']['value'];
                    }
                }
                
                $timestamp = Mage::getModel('core/date')->timestamp(time());
                if (!$id) {
                    $data['created_at'] = $timestamp;
                    $data['updated_at'] = $timestamp;
                          
                } else {
                    $data['updated_at'] = $timestamp;
                } 
                
                $categoryModel->setData($data)->setId($id);
                $categoryModel->save();
                                
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Category was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $categoryModel->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    /**
     * Delete Download Action
     * 
     */
    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){
            try {
                Mage::getModel('downloads/downloadscategory')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Category was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    /**
     * Mass Delete Sets Action
     * 
     */
    public function massDeleteAction()
    {
        $downloads = $this->getRequest()->getParam('downloadscategory', null);
        
        if (is_array($downloads) && sizeof($downloads) > 0) {
            try {
                foreach ($downloads as $id) {
                    Mage::getModel('downloads/downloadscategory')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d Category have been deleted', sizeof($downloads)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please Select Category'));
        }
        
        $this->_redirect('*/*');
    }
}
