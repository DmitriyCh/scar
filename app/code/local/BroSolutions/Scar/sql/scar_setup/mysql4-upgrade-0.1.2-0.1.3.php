<?php

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$cmsPageTable = $installer->getTable('blog/blog');


$connection->addColumn($cmsPageTable, 'post_image', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'nullable'  => true,
    'comment'   => 'Page image'
));

$installer->endSetup();
