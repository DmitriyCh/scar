<?php

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$cmsPageTable = $installer->getTable('cms_page');


$connection->addColumn($cmsPageTable, 'page_image', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'nullable'  => true,
    'comment'   => 'Page image'
));

$installer->endSetup();
