<?php

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$newsLetterTable = $installer->getTable('newsletter/subscriber');

$connection->addColumn($newsLetterTable,'name', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'nullable'  => true,
    'after'     => 'customer_id',
    'comment'   => 'Name'
));
$connection->addColumn($newsLetterTable,'country', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'nullable'  => true,
    'after'     => 'subscriber_email',
    'comment'   => 'Country'
));

$installer->endSetup();
