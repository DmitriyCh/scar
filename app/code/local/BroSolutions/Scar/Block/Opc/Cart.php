<?php
/**
 * Created by PhpStorm.
 * User: DEV5
 * Date: 28.04.2017
 * Time: 11:24
 */
class BroSolutions_Scar_Block_OPC_Cart extends Mage_Core_Block_Template
{
    public function getCustomOptions($item)
    {
        /**
         * @var Mage_Sales_Model_Quote_Item $item
         * @var Mage_Catalog_Model_Product $product
         */
        $product = $item->getProduct();
        $typeId = $product->getTypeId();
        if ($typeId != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return false;
        }
        $attributes = $product->getTypeInstance(true)
            ->getSelectedAttributesInfo($product);
        return $attributes;
    }
}