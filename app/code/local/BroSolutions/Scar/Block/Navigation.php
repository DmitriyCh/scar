<?php

/**
 * Created by PhpStorm.
 * User: DEV5
 * Date: 21.02.2017
 * Time: 17:32
 */
class BroSolutions_Scar_Block_Navigation extends Mage_Catalog_Block_Navigation
{
    public function getCurrentNeighborCategories()
    {
        $current_cat = Mage::registry('current_category');
        if ($current_cat->getParentId()) {
            $parentCat = Mage::getModel("catalog/category")->load($current_cat->getParentId());
            return $parentCat->getChildrenCategories();

        }

        return 0;
        /*if (null === $this->_currentChildCategories) {
            $layer = Mage::getSingleton('catalog/layer');
            $category = $layer->getCurrentCategory();
            $this->_currentChildCategories = $category->getChildrenCategories();
            $productCollection = Mage::getResourceModel('catalog/product_collection');
            $layer->prepareProductCollection($productCollection);
            $productCollection->addCountToCategories($this->_currentChildCategories);
        }
        return $this->_currentChildCategories;*/
    }

    public function isCategoryActive($category)
    {
        $current_cat = Mage::registry('current_category');
        if ($current_cat->getId() == $category->getId()) return true;

        return false;
    }

}