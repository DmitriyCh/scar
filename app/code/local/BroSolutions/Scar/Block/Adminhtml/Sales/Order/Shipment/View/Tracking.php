<?php

class BroSolutions_Scar_Block_Adminhtml_Sales_Order_Shipment_View_Tracking extends Mage_Adminhtml_Block_Sales_Order_Shipment_View_Tracking
{
    /**
     * Adds additional careers hardcoded
     *
     * @param $code string
     * @return string
     */
    public function getCarrierTitle($code)
    {
        $array = array('la_post' => 'La Poste', 'chronopost' => 'Chronopost', 'mondial_relay' => 'Mondial Relay');

        if ($carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($code)) {
            return $carrier->getConfigData('title');
        } elseif (in_array($code, array('la_post', 'chronopost', 'mondial_relay'))) {

            return Mage::helper('sales')->__($array[$code]);
        } else {
            return Mage::helper('sales')->__('Custom Value');
        }
    }
}
