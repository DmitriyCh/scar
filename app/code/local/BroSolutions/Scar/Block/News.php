<?php
/**
 * Created by PhpStorm.
 * User: DEV5
 * Date: 15.05.2017
 * Time: 12:55
 */

class BroSolutions_Scar_Block_News extends Mage_Core_Block_Template
{
    public function getLatestNewsItem()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $postCats = Mage::getModel('blog/api')->getPosts(array(1), array($storeId));
        return $postCats->getLastItem();
    }
}