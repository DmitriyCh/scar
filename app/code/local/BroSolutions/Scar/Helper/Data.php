<?php

class BroSolutions_Scar_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getBGImageUrl()
    {
        return Mage::getBaseUrl('media') . "brosol/" . Mage::getStoreConfig("scar/settings/bbimage");
    }

    public function getProdImageUrl()
    {

        return Mage::getBaseUrl('media') . "brosol/" . Mage::getStoreConfig("scar/settings/prodimage");
    }

    public function langAcronym($lang)
    {
        switch ($lang) {
            case "English":
                return "EN";
                break;
            case "French":
                return "FR";
                break;
            case "German":
                return "DE";
                break;
        }

        return "";
    }

    public function getApproxProductPrices($finalPrice)
    {
        $html = '(' . $this->__('Approx') . ' ';
        $storeId = Mage::app()->getStore()->getId();
        $currencyCode = Mage::getModel('core/config_data')
            ->getCollection()
            ->addFieldToFilter('path', 'currency/options/allow')
            ->getData();
        $currencies_array = explode(',', $currencyCode[0]['value']);
        $defaultCurrencyCode = Mage::app()->getStore($storeId)->getCurrentCurrencyCode();
        $from = Mage::app()->getStore()->getBaseCurrencyCode();
        foreach ($currencies_array as $currenciCode) {
            if ($currenciCode == $defaultCurrencyCode) {
                continue;
            }
            $to = Mage::getModel('directory/currency')->load($currenciCode);
            $val = Mage::helper('directory')->currencyConvert($finalPrice, $from, $to);
            $html .= $to->formatPrecision($val, 2, array(), false, false) . $currenciCode . ' / ';
        }
        $html = rtrim($html, ' / ') . ')';

        return $html;
    }

    public function getCareers($array)
    {
        return array_merge($array, array('la_post' => 'La Poste', 'chronopost' => 'Chronopost', 'mondial_relay' => 'Mondial Relay'));
    }
}
