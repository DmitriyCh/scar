<?php
/**
 * Created by PhpStorm.
 * User: DEV5
 * Date: 21.03.2017
 * Time: 12:06
 */
class BroSolutions_Scar_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $post = $this->getRequest()->getPost();
        if (empty($post))
        {
            $this->_redirectReferer(Mage::getUrl('*/*'));
            return;
        }
        $id  = str_replace("post_", "", $post['id']);
        $post = Mage::getModel("blog/post")->load($id);

        if ($post->getPostImage() == '' || $post->getPostImage() == null) {
            $post->setAbsoluteImgUrl("");
        } else {
            $post->setAbsoluteImgUrl(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $post->getPostImage());
        }

        $this->getResponse()->clearHeaders()->setHeader(
            'Content-type',
            'application/json'
        );
        $arr = array(
            'title' => $post->getTitle(),
            'content' => $post->getPostContent(),
            'img' => $post->getAbsoluteImgUrl(),
            'time' => Mage::getModel('core/date')->date('d.m.Y', $post->getCreatedTime()),
        );

        $this->getResponse()->setBody(json_encode($arr));
    }
}