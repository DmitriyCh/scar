<?php

/**
 * Created by PhpStorm.
 * User: DEV5
 * Date: 22.02.2017
 * Time: 14:10
 */
class BroSolutions_Scar_Model_Observer
{
    public function cmsField($observer)
    {
        $model = Mage::registry('cms_page');
        $form = $observer->getForm();
        $fieldset = $form->addFieldset('brosol_content_fieldset', array('legend' => Mage::helper('cms')->__('Image'), 'class' => 'fieldset-wide'));
        $fieldset->addField('page_image', 'image', array(
            'name' => 'page_image',
            'label' => Mage::helper('cms')->__('Page Image'),
            'title' => Mage::helper('cms')->__('Page Image'),
            'disabled' => false,
            //set field value
            'value' => $model->getPageImage()
        ));
    }

    public function blogField($observer)
    {
        $model = Mage::getSingleton("blog/blog")->load(Mage::app()->getRequest()->getParam("id"));
        $form = $observer->getForm();
        $fieldset = $form->addFieldset('brosol_post_image', array('legend' => Mage::helper('cms')->__('Image'), 'class' => 'fieldset-wide'));
        $fieldset->addField('post_image', 'image', array(
            'name' => 'post_image',
            'label' => Mage::helper('cms')->__('Post Image'),
            'title' => Mage::helper('cms')->__('Post Image'),
            'disabled' => false,
            'value' => $model->getPostImage()
        ));
    }

    public function addFormEnctype($observer)
    {
        try {
            $block = $observer->getEvent()->getBlock();
            if (($block instanceof Mage_Adminhtml_Block_Cms_Page_Edit_Form) || $block instanceof  AW_Blog_Block_Manage_Blog_Edit_Form) {
                $form = $block->getForm();
                $form->setData('enctype', 'multipart/form-data');
                $form->setUseContainer(true);
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function savePage($observer)
    {
        try {
            $model = $observer->getEvent()->getPage();
            $request = $observer->getEvent()->getRequest();

            if (isset($_FILES['page_image']['name']) && $_FILES['page_image']['name'] != '') {
                $uploader = new Varien_File_Uploader('page_image');

                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);

                // Set media as the upload dir
                $media_path = Mage::getBaseDir('media') . DS . 'page_image' . DS;

                // Set thumbnail name
                $file_name = 'cms_';

                // Upload the image
                $uploader->save($media_path, $file_name . $_FILES['page_image']['name']);

                $data['page_image'] = 'page_image' . DS . $file_name . $_FILES['page_image']['name'];

                // Set thumbnail name
                //$data['page_image'] = $data['page_image'];
                $model->setPageImage($data['page_image']);
            } else {
                $data = $request->getPost();
                if ($data['page_image']['delete'] == 1) {
                    $data['page_image'] = '';
                    $model->setPageImage($data['page_image']);
                } else {
                    unset($data['page_image']);
                    $model->setPageImage(implode($request->getPost('page_image')));
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function saveBlog($observer)
    {
        try {
            $model = $observer->getEvent()->getPost();
            $request = $observer->getEvent()->getRequest();

            if (isset($_FILES['post_image']['name']) && $_FILES['post_image']['name'] != '') {
                $uploader = new Varien_File_Uploader('post_image');

                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);

                // Set media as the upload dir
                $media_path = Mage::getBaseDir('media') . DS . 'post_image' . DS;

                // Set thumbnail name
                $file_name = 'cms_';

                // Upload the image
                $uploader->save($media_path, $file_name . $_FILES['post_image']['name']);

                $data['post_image'] = 'post_image' . DS . $file_name . $_FILES['post_image']['name'];

                // Set thumbnail name
                //$data['page_image'] = $data['page_image'];
                $model->setPostImage($data['post_image']);
            } else {
                $data = $request->getPost();
                if ($data['post_image']['delete'] == 1) {
                    $data['post_image'] = '';
                    $model->setPageImage($data['post_image']);
                } else {
                    unset($data['post_image']);
                    $model->setPageImage(implode($request->getPost('post_image')));
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
