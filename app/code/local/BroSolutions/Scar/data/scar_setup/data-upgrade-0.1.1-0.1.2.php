<?php

$installer = $this;
$installer->startSetup();

$data = file_get_contents('app/code/local/BroSolutions/Scar/data/scar_setup/data.sql');
$installer->run($data);

$installer->endSetup();