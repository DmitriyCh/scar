<?php
class BroSolutions_CatPage_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View
{
    public function getChildCategories()
    {
        $currentCategory = $this->getCurrentCategory();
        $childrenCollection = Mage::getResourceModel('catalog/category_collection')
            ->addIsActiveFilter()->addNameToResult()->addUrlRewriteToResult()->setOrder('position', Varien_Db_Select::SQL_ASC)
            ->addAttributeToSelect('image')
            ->addIdFilter($currentCategory->getChildren());
        return $childrenCollection;
    }

    public function isChildrensOnlyMode()
    {
        return $this->getCurrentCategory()->getDisplayMode() == BroSolutions_CatPage_Helper_Data::CATEGORY_DISPLAY_MODE_CHILDREN_ONLY;
    }
}