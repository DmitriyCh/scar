<?php

class BroSolutions_Riders_Model_Riders extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('riders/riders');
    }
}
