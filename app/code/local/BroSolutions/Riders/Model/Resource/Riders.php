<?php

class BroSolutions_Riders_Model_Resource_Riders extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        $this->_init('riders/table_riders', 'entity_id');
    }
}
