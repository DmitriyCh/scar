<?php

class BroSolutions_Riders_Model_Resource_Riders_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    /**
     * Initialization here
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('riders/riders');
    }
}
