<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableRidersName = $installer->getTable('riders/table_riders');
$installer->getConnection()->dropTable($tableRidersName);

$tableRiders = $installer->getConnection()
        ->newTable($tableRidersName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'auto_increment' => true,
            'unsigned' => true,
            'nullable'=> false,
            'primary' => true,
        ), 'Entity ID')
        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Title')
        ->addColumn('rider_image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Rider Image')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_CHAR, 10, array(
            'nullable' => false,
        ), 'Type')
        ->addColumn('link', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Link')
        ->addColumn('status',  Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'unsigned' => true,
            'nullable'=> false,
            'default' => true
        ), 'Status')
        ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
            'nullable' => false,
        ), 'Description')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Updated At');

$tableRiders->setOption('type', 'InnoDB');
$tableRiders->setOption('charset', 'utf8');
$installer->getConnection()->createTable($tableRiders);

$installer->endSetup();

