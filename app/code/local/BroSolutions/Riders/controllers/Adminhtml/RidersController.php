<?php

class BroSolutions_Riders_Adminhtml_RidersController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('riders');
    }
    
    /**
     * Index Action
     * 
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('riders');
        $contentBlock = $this->getLayout()->createBlock('riders/adminhtml_riders');
        $this->_addContent($contentBlock);
        $this->renderLayout();
    }
    
    /**
     * New Rider Action
     * 
     */
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    /**
     * Edit Rider Action
     * 
     */
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_rider', Mage::getModel('riders/riders')->load($id));
        
        $this->loadLayout();
        $this->_setActiveMenu('riders');
        
        $leftBlock = $this->getLayout()->createBlock('riders/adminhtml_riders_edit_tabs');
        $this->_addLeft($leftBlock);
        
        $contentBlock = $this->getLayout()->createBlock('riders/adminhtml_riders_edit');
        $this->_addContent($contentBlock);
        
        $this->renderLayout();
    }
    
    /**
     * Upload rider image
     * 
     * @param array $file || $_FILES['rider_image']
     * @return string
     */
    protected function _uploadRiderImage($file)
    {
        $media_path = Mage::getBaseDir('media') . DS . 'riders';
        try {
            $uploader = new Varien_File_Uploader($file);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(true);
            
            $arrName = explode('.', $file['name']);
            $ext = array_pop($arrName);
            $fileName = implode('.', $arrName). '_' . date('Y_m_d_H_i_s') . '.' . $ext;
            $imgFilename = strtolower(str_replace(' ', '', $fileName));
            
            $result = $uploader->save($media_path, $imgFilename);
            $riderImage = 'riders' . $result['file'];
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving rider image.'));
        }
        
        return $riderImage;
    }
    
    /**
     * Save Rider Item Action
     * 
     * @return void
     */
    public function saveAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('riders');
                $riderModel = Mage::getModel('riders/riders');

                if(!empty($id) && isset($data['rider_image']['delete']) && $data['rider_image']['delete'] == 1) {
                    $imagePath = $riderModel->load($id)->getRiderImage();
                    $helper->deleteImage($imagePath);
                    $data['rider_image'] = '';
                }
                
                if(isset($_FILES['rider_image']) && $_FILES['rider_image']['error'] == 0){
                    if(!empty($id)){
                        $imagePath = $riderModel->load($id)->getRiderImage();
                        $helper->deleteImage($imagePath);
                    }
                    
                    $data['rider_image'] = $this->_uploadRiderImage($_FILES['rider_image']);
                }
                else{
                    if(isset($data['rider_image']['value'])){
                        $data['rider_image'] = $data['rider_image']['value'];
                    }
                }
                
                if(!$data['rider_image']){
                    Mage::getSingleton('adminhtml/session')->addError('Image is a required field.');
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $id));
                    return;
                }
                
                $timestamp = Mage::getModel('core/date')->timestamp(time());
                if (!$id) {
                    $data['created_at'] = $timestamp;
                    $data['updated_at'] = $timestamp;
                          
                } else {
                    $data['updated_at'] = $timestamp;
                } 
                
                $riderModel->setData($data)->setId($id);
                $riderModel->save();
                                
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Rider was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $riderModel->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    /**
     * Delete Rider Action
     * 
     */
    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){
            try {
                Mage::getModel('riders/riders')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Rider was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    /**
     * Mass Delete Sets Action
     * 
     */
    public function massDeleteAction()
    {
        $riders = $this->getRequest()->getParam('riders', null);
        
        if (is_array($riders) && sizeof($riders) > 0) {
            try {
                foreach ($riders as $id) {
                    Mage::getModel('riders/riders')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d Riders have been deleted', sizeof($riders)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please Select Riders'));
        }
        
        $this->_redirect('*/*');
    }
}
