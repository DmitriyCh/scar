<?php

class BroSolutions_Riders_Block_Adminhtml_Riders extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Internal constructor
     *
     */
    public function __construct()
    {
        $helper = Mage::helper('riders');
        $this->_blockGroup = 'riders';
        $this->_controller = 'adminhtml_riders';

        $this->_headerText = $helper->__('Riders Management');
        $this->_addButtonLabel = $helper->__('Add New Rider');
        
        parent::__construct();
    }
}