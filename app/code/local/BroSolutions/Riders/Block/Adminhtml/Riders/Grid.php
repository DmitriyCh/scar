<?php

class BroSolutions_Riders_Block_Adminhtml_Riders_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('ridersGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Prepare grid collection object
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('riders/riders')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * Prepare Grid Columns
     * 
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('riders');
        
        $this->addColumn('entity_id', array(
            'header'    => $helper->__('Rider ID'),
            'index'     => 'entity_id',
            'align'     => 'right',
            'width'     => '50px',
        ));
        
        $this->addColumn('rider_image', array(
            'header'    => $helper->__('Image'),
            'index'     => 'rider_image',
            'align'     => 'center',
            'width'     => '70px',
            'renderer' => 'riders/adminhtml_riders_edit_grid_renderer_image'
        ));

        $this->addColumn('type', array(
            'header'    => $helper->__('Type'),
            'index'     => 'type',
            'align'     => 'left',
            'width'     => '50px',
            'type'      => 'options',
            'options'   => array(
                'small' => 'Small',
                'large' => 'Large',
            ),
        ));
        
        $this->addColumn('title', array(
            'header'    => $helper->__('Title'),
            'index'     => 'title',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('link', array(
            'header'    => $helper->__('Link'),
            'index'     => 'link',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        /*
        $this->addColumn('description', array(
            'header'    => $helper->__('Description'),
            'index'     => 'description',
            'type'      => 'text',
            'align'     => 'left',
        ));
        */
        
        $this->addColumn('status', array(
            'header'    => $helper->__('Status'),
            'index'     => 'status',
            'align'     => 'left',
            'width'     => '100px',
            'type'      => 'options',
            'options'   => array(
                '1' => 'Enabled',
                '0' => 'Disabled',
            ),
        ));
        
        $this->addColumn('created_at', array(
            'header'    => $helper->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('updated_at', array(
            'header'    => $helper->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('action', array(
            'header'    =>  $helper->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => array(
                array(
                    'caption'   => $helper->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'id'
                ),
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
            'align'     => 'center',
        ));
        
        return parent::_prepareColumns();
    }
    
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addJs('brosolutions/tooltip.js');
        parent::_prepareLayout();
    }

        /**
     * Get Row Url
     * 
     * @param BroSolutions_Riders_Model_Riders $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    /**
     * Prepare Massaction
     * 
     * @return BroSolutions_Riders_Block_Adminhtml_Riders_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('riders');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
