<?php

class BroSolutions_Riders_Block_Adminhtml_Riders_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('riders');
        $riderModel = Mage::registry('current_rider');
        
        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));
                
        $fieldset->addField('title', 'text', array(
            'label'    => $helper->__('Title'),
            'required' => true,
            'name'     => 'title',
        ));
        
        $fieldset->addField('rider_image', 'image', array(
            'name' => 'rider_image',
            'label' => $helper->__('Image'),
            'title' => $helper->__('Image'),
            'required' => true,
        ));
        
        $fieldset->addField('type', 'select', array(
            'label' => $helper->__('Type'),
            'name' => 'type',
            'values' => array(
                array(
                    'value' => 'small',
                    'label' => $helper->__('Small'),
                ),
                array(
                    'value' => 'large',
                    'label' => $helper->__('Large'),
                ),
            ),
            'required' => true,
        ));
        
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => '1',
                    'label' => $helper->__('Enabled'),
                ),
                array(
                    'value' => '0',
                    'label' => $helper->__('Disabled'),
                ),
            ),
            'required' => true,
        ));
        
        $fieldset->addField('link', 'text', array(
            'label'    => $helper->__('Link'),
            'name'     => 'link',
            'required' => false,
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('description', 'editor', array(
            'label'    => $helper->__('Description'),
            'title'    => $helper->__('Description'),
            'style'    => 'height:300px; width: 550px;',
            'required' => false,
            'name'     => 'description',
            'wysiwyg'  => true,
            'config'   => $wysiwygConfig,
        ));
        
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        }
        else {
            $form->setValues($riderModel->getData());
        }
        
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
