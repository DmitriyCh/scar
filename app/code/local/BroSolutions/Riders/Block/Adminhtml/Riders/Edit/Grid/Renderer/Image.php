<?php

class BroSolutions_Riders_Block_Adminhtml_Riders_Edit_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    
    /**
     * 
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row) {
        $imageId = $row->getId();
        //$imageName = Mage::getModel('riders/riders')->load($imageId)->getRiderImage();
        $imageName = $row->getRiderImage();
        
        if(!$imageName) 
            return Mage::helper('riders')->__('No image');
        
        $imageUrl = Mage::helper('riders')->getImageUrl($imageName);
        
        return  '<img id="rider_image_' . $imageId . '" src="' . $imageUrl . '" width="70px" height="70px"/>'.
                '<script type="text/javascript">
                    new Tooltip("rider_image_'.$imageId.'", "'.$imageUrl.'");
                    $$(".tooltip img").each(function(item){
                        item.style.width="300px";
                        item.style.height="300px";
                    });
                </script>'
                
        ;
    }

}
