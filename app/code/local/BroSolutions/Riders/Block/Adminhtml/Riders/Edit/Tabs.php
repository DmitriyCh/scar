<?php

class BroSolutions_Riders_Block_Adminhtml_Riders_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('riders');
        $this->setId('riders_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Rider Information'));
    }
    
    /**
     * Preparing layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('riders');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('riders/adminhtml_riders_edit_tabs_general')->toHtml(),
        ));
        
        return parent::_prepareLayout();
    }
}
