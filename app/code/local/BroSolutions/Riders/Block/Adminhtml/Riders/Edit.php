<?php

class BroSolutions_Riders_Block_Adminhtml_Riders_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('riders');
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'riders';
        $this->_controller = 'adminhtml_riders';
        
        $this->_updateButton('save', 'label', $helper->__('Save Rider'));
        $this->_updateButton('delete', 'label', $helper->__('Delete Rider'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => $helper->__('Save Rider And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -10);

        
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * Get Header Text
     * 
     * @return string
     */
    public function getHeaderText()
    {
        $helper = Mage::helper('riders');
        $riderModel = Mage::registry('current_rider');

        if ($riderModel->getId()) {
            return $helper->__("Edit Rider ID '%s'", $riderModel->getId());
        } else {
            return $helper->__("Add New Rider");
        }
    }
    
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
}
