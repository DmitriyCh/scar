<?php

class BroSolutions_Riders_Block_Riderslist extends Mage_Core_Block_Template
{
    /**
     * Get Riders Collection
     * 
     * @return BroSolutions_Riders_Model_Resource_Riders_Collection
     */
    public function getRiders()
    {
        $collection = Mage::getModel('riders/riders')->getCollection();
        $collection->addFieldToFilter('status' , array("eq" => 1));

        return $collection;
    }
    
    public function _prepareLayout()
    {
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
         
        $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('riders')->__('Home'), 
                    'title' => Mage::helper('riders')->__('Home Page'), 
                    'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
                )
        );
        
        $breadcrumbs->addCrumb('riders', array(
                    'label' => Mage::helper('riders')->__('Riders'), 
                    'title' => Mage::helper('riders')->__('Riders'), 
                    //'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'riders',
                )
        );
        
        parent::_prepareLayout();
    }
}
