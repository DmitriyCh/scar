<?php

class BroSolutions_Riders_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Delete image
     * 
     * @param string $imagePath
     * @return boolean
     */
    public function deleteImage($imagePath)
    {
        $imagePath = Mage::getBaseDir("media") . DS . str_replace("/", DS, $imagePath);
        
        if (is_file($imagePath)) {
            if (unlink($imagePath)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get Image Url
     * 
     * @param string Image Name
     * @return string image url
     */
    public function getImageUrl($fileName = null)
    {
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        if ($fileName) {
            return $url . $fileName;
        } else {
            return $url;
        }
    }
}
