<?php

class BroSolutions_Dealers_Model_Resource_Dealers extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        $this->_init('dealers/table_dealers', 'entity_id');
    }
}
