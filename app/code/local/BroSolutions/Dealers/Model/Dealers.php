<?php

class BroSolutions_Dealers_Model_Dealers extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resources
     * 
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('dealers/dealers');
    }
}
