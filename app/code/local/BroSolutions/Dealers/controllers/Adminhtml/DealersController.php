<?php

class BroSolutions_Dealers_Adminhtml_DealersController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('dealers');
    }
    
    /**
     * Index Action
     * 
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('dealers');
        $contentBlock = $this->getLayout()->createBlock('dealers/adminhtml_dealers');
        $this->_addContent($contentBlock);
        $this->renderLayout();
    }
    
    /**
     * New Dealer Action
     * 
     */
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    /**
     * Edit Dealer Action
     * 
     */
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_dealer', Mage::getModel('dealers/dealers')->load($id));
        
        $this->loadLayout();
        $this->_setActiveMenu('dealers');
        
        $leftBlock = $this->getLayout()->createBlock('dealers/adminhtml_dealers_edit_tabs');
        $this->_addLeft($leftBlock);
        
        $contentBlock = $this->getLayout()->createBlock('dealers/adminhtml_dealers_edit');
        $this->_addContent($contentBlock);
        
        $this->renderLayout();
    }
        
    /**
     * Save Dealer Item Action
     * 
     * @return void
     */
    public function saveAction()
    {
        $id = $this->getRequest()->getParam('id');
        if ($data = $this->getRequest()->getPost()) {
            try {
                $helper = Mage::helper('dealers');
                $dealerModel = Mage::getModel('dealers/dealers');

                $timestamp = Mage::getModel('core/date')->timestamp(time());
                if (!$id) {
                    $data['created_at'] = $timestamp;
                    $data['updated_at'] = $timestamp;
                          
                } else {
                    $data['updated_at'] = $timestamp;
                } 
                
                $dealerModel->setData($data)->setId($id);
                $dealerModel->save();
                                
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Dealer was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $dealerModel->getId()));
                    return;
                }

                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    /**
     * Delete Dealer Action
     * 
     */
    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){
            try {
                Mage::getModel('dealers/dealers')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Dealer was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    /**
     * Mass Delete Sets Action
     * 
     */
    public function massDeleteAction()
    {
        $dealers = $this->getRequest()->getParam('dealers', null);
        
        if (is_array($dealers) && sizeof($dealers) > 0) {
            try {
                foreach ($dealers as $id) {
                    Mage::getModel('dealers/dealers')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d Dealers have been deleted', sizeof($dealers)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please Select Dealers'));
        }
        
        $this->_redirect('*/*');
    }
}
