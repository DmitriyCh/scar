<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tableDealersName = $installer->getTable('dealers/table_dealers');
$installer->getConnection()->dropTable($tableDealersName);

$tableDealers = $installer->getConnection()
        ->newTable($tableDealersName)
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'auto_increment' => true,
            'unsigned' => true,
            'nullable'=> false,
            'primary' => true,
        ), 'Entity ID')
        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Title')
        ->addColumn('country', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Country')
        ->addColumn('address', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Address')
        ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Phone')
        ->addColumn('fax', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Fax')
        ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Email')
        ->addColumn('link', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Link')
        ->addColumn('latitude', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Latitude')
        ->addColumn('longitude', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
        ), 'Longitude')
        ->addColumn('status',  Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
            'unsigned' => true,
            'nullable'=> false,
            'default' => true
        ), 'Status')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
            'nullable' => false,
        ), 'Updated At');

$tableDealers->setOption('type', 'InnoDB');
$tableDealers->setOption('charset', 'utf8');
$installer->getConnection()->createTable($tableDealers);

$installer->endSetup();

