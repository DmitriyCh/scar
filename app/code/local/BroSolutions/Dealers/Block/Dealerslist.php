<?php

class BroSolutions_Dealers_Block_Dealerslist extends Mage_Core_Block_Template
{
    /**
     * Get Dealers Collection
     * 
     * @return BroSolutions_Dealers_Model_Resource_Dealers_Collection
     */
    public function getDealers()
    {
        $collection = Mage::getModel('dealers/dealers')->getCollection();
        $collection->addFieldToFilter('status' , array("eq" => 1));
        $collection->setOrder('country', 'ASC');
        return $collection;
    }
    
    public function _prepareLayout()
    {
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
         
        $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('dealers')->__('Home'), 
                    'title' => Mage::helper('dealers')->__('Home Page'), 
                    'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
                )
        );
        
        $breadcrumbs->addCrumb('dealers', array(
                    'label' => Mage::helper('dealers')->__('Worldwide distributors'), 
                    'title' => Mage::helper('dealers')->__('Worldwide distributors'), 
                    //'link'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . 'dealers',
                )
        );
        
        parent::_prepareLayout();
    }
}
