<?php

class BroSolutions_Dealers_Block_Adminhtml_Dealers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('dealers');
        
        $this->_objectId = 'id';
        $this->_blockGroup = 'dealers';
        $this->_controller = 'adminhtml_dealers';
        
        $this->_updateButton('save', 'label', $helper->__('Save Dealer'));
        $this->_updateButton('delete', 'label', $helper->__('Delete Dealer'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => $helper->__('Save Dealer And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -10);

        
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * Get Header Text
     * 
     * @return string
     */
    public function getHeaderText()
    {
        $helper = Mage::helper('dealers');
        $dealerModel = Mage::registry('current_dealer');

        if ($dealerModel->getId()) {
            return $helper->__("Edit Dealer ID '%s'", $dealerModel->getId());
        } else {
            return $helper->__("Add New Dealer");
        }
    }
    
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }
}
