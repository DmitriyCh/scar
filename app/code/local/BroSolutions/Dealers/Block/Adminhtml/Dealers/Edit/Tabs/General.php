<?php

class BroSolutions_Dealers_Block_Adminhtml_Dealers_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('dealers');
        $dealerModel = Mage::registry('current_dealer');
        
        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));
                
        $fieldset->addField('title', 'text', array(
            'label'    => $helper->__('Title'),
            'required' => true,
            'name'     => 'title',
        ));
        
        $fieldset->addField('country', 'text', array(
            'label'    => $helper->__('Country'),
            'required' => false,
            'name'     => 'country',
        ));
        
        $fieldset->addField('address', 'text', array(
            'label'    => $helper->__('Address'),
            'required' => false,
            'name'     => 'address',
        ));
        
        $fieldset->addField('phone', 'text', array(
            'label'    => $helper->__('Phone'),
            'required' => true,
            'name'     => 'phone',
        ));
        
        $fieldset->addField('fax', 'text', array(
            'label'    => $helper->__('Fax'),
            'required' => false,
            'name'     => 'fax',
        ));
        
        $fieldset->addField('email', 'text', array(
            'label'    => $helper->__('Email'),
            'required' => false,
            'name'     => 'email',
            'class'    => 'validate-email',
        ));
        
        $fieldset->addField('link', 'text', array(
            'label'    => $helper->__('Link'),
            'name'     => 'link',
            'required' => false,
        ));

        $fieldset->addField('latitude', 'text', array(
            'label'    => $helper->__('Latitude'),
            'required' => false,
            'name'     => 'latitude',
        ));
        
        $fieldset->addField('longitude', 'text', array(
            'label'    => $helper->__('Longitude'),
            'required' => false,
            'name'     => 'longitude',
        ));
        
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => '1',
                    'label' => $helper->__('Enabled'),
                ),
                array(
                    'value' => '0',
                    'label' => $helper->__('Disabled'),
                ),
            ),
            'required' => true,
        ));
        
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        }
        else {
            $form->setValues($dealerModel->getData());
        }
        
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
}
