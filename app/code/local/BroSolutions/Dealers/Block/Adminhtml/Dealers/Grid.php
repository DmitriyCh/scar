<?php

class BroSolutions_Dealers_Block_Adminhtml_Dealers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('dealersGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    
    /**
     * Prepare grid collection object
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('dealers/dealers')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /**
     * Prepare Grid Columns
     * 
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('dealers');
        
        $this->addColumn('entity_id', array(
            'header'    => $helper->__('Dealer ID'),
            'index'     => 'entity_id',
            'align'     => 'right',
            'width'     => '50px',
        ));
        
        $this->addColumn('title', array(
            'header'    => $helper->__('Title'),
            'index'     => 'title',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('country', array(
            'header'    => $helper->__('Country'),
            'index'     => 'country',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('address', array(
            'header'    => $helper->__('Address'),
            'index'     => 'address',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('phone', array(
            'header'    => $helper->__('Phone'),
            'index'     => 'phone',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('fax', array(
            'header'    => $helper->__('Fax'),
            'index'     => 'fax',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('email', array(
            'header'    => $helper->__('Email'),
            'index'     => 'email',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('link', array(
            'header'    => $helper->__('Link'),
            'index'     => 'link',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('latitude', array(
            'header'    => $helper->__('Latitude'),
            'index'     => 'latitude',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('longitude', array(
            'header'    => $helper->__('Longitude'),
            'index'     => 'longitude',
            'type'      => 'text',
            'align'     => 'left',
        ));
        
        $this->addColumn('status', array(
            'header'    => $helper->__('Status'),
            'index'     => 'status',
            'align'     => 'left',
            'width'     => '100px',
            'type'      => 'options',
            'options'   => array(
                '1' => 'Enabled',
                '0' => 'Disabled',
            ),
        ));
        
        $this->addColumn('created_at', array(
            'header'    => $helper->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('updated_at', array(
            'header'    => $helper->__('Updated At'),
            'index'     => 'updated_at',
            'type'      => 'text',
            'align'     => 'left',
            'width'     => '130px',
        ));
        
        $this->addColumn('action', array(
            'header'    =>  $helper->__('Action'),
            'width'     => '50px',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => array(
                array(
                    'caption'   => $helper->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'id'
                ),
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
            'align'     => 'center',
        ));
        
        return parent::_prepareColumns();
    }
    
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }

        /**
     * Get Row Url
     * 
     * @param BroSolutions_Dealers_Model_Dealers $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
    /**
     * Prepare Massaction
     * 
     * @return BroSolutions_Dealers_Block_Adminhtml_Dealers_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('dealers');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
