<?php

class BroSolutions_Dealers_Block_Adminhtml_Dealers_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * construct
     * 
     */
    public function __construct()
    {
        parent::__construct();
        
        $helper = Mage::helper('dealers');
        $this->setId('dealers_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Dealer Information'));
    }
    
    /**
     * Preparing layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('dealers');

        $this->addTab('general_section', array(
            'label' => $helper->__('General Information'),
            'title' => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('dealers/adminhtml_dealers_edit_tabs_general')->toHtml(),
        ));
        
        return parent::_prepareLayout();
    }
}
