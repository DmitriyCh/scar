<?php

class BroSolutions_Dealers_Block_Adminhtml_Dealers extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Internal constructor
     *
     */
    public function __construct()
    {
        $helper = Mage::helper('dealers');
        $this->_blockGroup = 'dealers';
        $this->_controller = 'adminhtml_dealers';

        $this->_headerText = $helper->__('Dealers Management');
        $this->_addButtonLabel = $helper->__('Add New Dealer');
        
        parent::__construct();
    }
}