jQuery(function($){
    masonryLoaded();
    setTimeout(masonryLoaded(),3000);
    function masonryLoaded(){
        $('.raider-grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: false,
            columnWidth: '.rider-gallery__sizer'
        });
    }
    $('[data-parts-finder-trigger]').on('click', function (e) {
        $('[data-parts-finder]').toggleClass('is-visible');
        e.preventDefault();
    });
    $('body').on('click', 'a', function(){
        if($(this).data('featherlight')){
            var t = $(this).find("img").attr("alt");
            $('.featherlight-content').prepend('<div class="featherlight-caption">'+t+'</div>');
        }
    });

    $('body .sidebar-nav__list').on('click', '.scar-year', function(){
        if ($(this).attr('data-year') != undefined){
           if($(this).attr('data-year').length){
               var currentAccordeon =  $(this).next('.blog-drop');
               if(currentAccordeon.hasClass('open-accordeon')){
                   $('.blog-drop').removeClass('open-accordeon').slideUp(300);
                   currentAccordeon.removeClass('open-accordeon').slideUp(300);
               } else {
                   $('.blog-drop').removeClass('open-accordeon').slideUp(300);
                   currentAccordeon.addClass('open-accordeon').slideDown(300);
               }
           }
        }
    });
});
